/* ================================================================================
 * Web App named "Progetto AmbienteParco" | Code name: PCS_PAP_2020-2021
 * --------------------------------------------------------------------------------
 * This WebApp was started to make available a simple way to booking activities for
 * AmbienteParco users by Web or native App (if it'll be possible for Android/iOS).
 * The development was initially based on a larger project that would use Moodle as
 * the system for educational and interactive contents and activities.
 * The actual website in WordPress could be moved on a self hosted version.
 * Finally a WebApp. embedded in Moodle and WordPress, and then as a native App for
 * Android and iOS devices, would be the bridge for users registering and booking.
 * In december 2020, due to Covid-19 lockdown, Moodle and WordPress development was
 * suspended in favour of the WebApp develpment, currently active.
 * --------------------------------------------------------------------------------
 * This WebApp use these framework, library, script and projects:
 * Bootstrap = https://getbootstrap.com/
 * DayPilot = https://daypilot.org
 * Font Awesome = https://fontawesome.com
 * JQuery = https://jquery.org
 * DataTables = https://www.datatables.net
 * PHPLiteAdmin = https://phpliteadmin.org
 * PHPQRCode = http://phpqrcode.sourceforge.net
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021 - ocdl.it/pap - Released 2020-09-22 - Updated 2021-05-07 20.39
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */

<div style="text-align:center;"><img src="img/webapp.png" /></div>
``````
