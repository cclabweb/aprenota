<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage in "database.sqlite" the data in table "ap_attivita":
 * Field					Type		!N	Value	Key
 * ------------------------+-----------+---+-------+----
 * idEvento					INTEGER		No	None	No
 * DataOraInizio			DATETIME	No	None	No
 * DataOraFine				DATETIME	No	None	No
 * idLuogo					INTEGER		No	None	No
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - Action = Create, Retrieve (One-RowID), Update (RowID) or Delete (RowID)
 * then comes two form:
 * - Retrieve All, when no action (DataTable: responsive, search, sort, pagination)
 * - Retrieve One-RowID for Create, Update, Delete actions with details, validation
 * + toggle show future/past activities
 * + added state save to search function so filters will be permanently
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}

		// ################################################################################ CPS. Title-Table-Name and 5 SQL Queries (CR12UD)
		$scp_Name ="Calendario";
		$scp_Table="ap_attivita"; // .$scp_Name;
		$sql_create = "INSERT INTO $scp_Table (idEvento, DataOraInizio, DataOraFine, idLuogo, Posti) VALUES ('".$_GET['idEvento']."', '".$_GET['DataOraInizio']."', '".$_GET['DataOraFine']."', '".$_GET['idLuogo']."', '".$_GET['Posti']."')";
		$sql_duplicate = "INSERT INTO $scp_Table SELECT * FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
		$sql_retrieve1 = "SELECT rowid, * FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
		$sql_update = "UPDATE $scp_Table SET idEvento = '".$_GET['idEvento']."', DataOraInizio = '".$_GET['DataOraInizio']."', DataOraFine = '".$_GET['DataOraFine']."', idLuogo = '".$_GET['idLuogo']."', Posti = '".$_GET['Posti']."' WHERE rowid = '".$_GET['RowID']."'";
		$sql_delete = "DELETE FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
		$sql_retrieve2 = "SELECT a.rowid, CASE WHEN p.idAttivita IS NULL THEN '<a href=\"ap_attivita.php?action=create-from&idEvento='||a.idEvento||'&idAttivita='||a.rowid||'&idOperatore=0\">Crea</a>' ELSE a.rowid END AS Crea,
				a.idEvento, e.Descrizione AS Evento, e.Note, a.DataOraInizio, a.DataOraFine, 
				a.idLuogo, a.Posti, l.Descrizione AS Luogo, l.Posti AS LuogoPosti FROM $scp_Table AS a 
					JOIN ap_eventi AS e on e.rowid=a.idEvento 
					JOIN ap_luoghi AS l on l.rowid=a.idLuogo 
					LEFT JOIN ap_prenotazioni AS p ON p.idattivita = a.rowid
				WHERE e.idTipo < '3'
				ORDER BY DataOraInizio DESC"; // 2021-05-18, CPS | ".$_SESSION['idOperatore']."
		// ################################################################################

		include 'ap_header.php'; ?>
		<title><?=ucfirst($scp_Name)?></title>
		<script>
			var a = document.getElementById("menu-<?=$ap_Name?>");
			a.classList.add("active");  
		</script>
		<!--
		<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
		// -->

		<!-- script src="https://www.jqueryscript.net/demo/DataTables-Jquery-Table-Plugin/media/js/jquery.js"></script>
		<script src="https://www.jqueryscript.net/demo/DataTables-Jquery-Table-Plugin/media/js/jquery.dataTables.js"></script>
		<link href="https://www.jqueryscript.net/demo/DataTables-Jquery-Table-Plugin/media/css/jquery.dataTables.css" rel="stylesheet">
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script --><!-- selectable 1/2 -->
		<!-- script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script --><!-- selectable 2/2 -->

	<?php include 'ap_menu.php'; ?>

			<h2 class="alert alert-secondary"><?=ucfirst($scp_Name)?></h2>
			<?php
			include 'ap_sqlite.php';
			/* ===== CREATE-FROM */
			if (isset($_GET['action']) && $_GET['action']=="create-from") {
				$sql_create2 = "INSERT INTO ap_prenotazioni (idEvento, idAttivita, idOperatore, idGruppo, idUtente, DataOraPresenza, Presenze, Importo, Cognome, Nome, Email, Telefono) 
							VALUES ('".$_GET['idEvento']."', '".$_GET['idAttivita']."', '".$_GET['idOperatore']."', '', '', '', '', '', '', '', '', '')";
				$dbs->exec($sql_create2);
				$idAttivita = $dbs->lastInsertRowID();
				echo "<script>alert('Prenotazione '+$idAttivita+' aggiunta.');</script>";
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== CREATE */
			if (isset($_GET['action']) && $_GET['action']=="create-save") {
				$dbs->exec($sql_create);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DUPLICATE */
			if (isset($_GET['action']) && $_GET['action']=="duplicate-save") {
				$duplicate=(isset($_GET['duplicate'])?$_GET['duplicate']:0);
				/* echo $_GET['duplicate']."<br>".$duplicate."<hr>";
				exit; */
				switch ($duplicate) {
					case 0; // Duplicate only the current single Activity without modification
						$sql_duplicate = "INSERT INTO $scp_Table SELECT * FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
						$dbs->exec($sql_duplicate);
						break;
					case 1; // Duplica solamente questa singola Attività fra 15 minuti
						$period = "15 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 2; // Duplica solamente questa singola Attività fra 30 minuti
						$period = "30 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 3; // Duplica solamente questa singola Attività fra 1 ora
						$period = "1 hour";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 4; // Duplica solamente questa singola Attività fra 1 giorno
						$period = "1 day";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 5; // Duplica solamente questa singola Attività fra 2 giorni
						/* $period = "2 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate); */
						break;
					case 6; // Duplica solamente questa singola Attività fra 1 settimana
						$period = "7 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 7; // Duplica solamente questa singola Attività fra 4 settimane
						/* $period = "28 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate); */
						break;
					case 8; // Duplica tutte le Attività in questi Evento-Data fra 1 giorno
						$period = "1 day";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 9; // Duplica tutte le Attività in questi Evento-Data fra 3 giorni
						$period = "3 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 10; // Duplica tutte le Attività in questi Evento-Data fra 1 settimana
						$period = "7 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 11; // Duplica tutte le Attività in questi Evento-Data fra 4 settimane
						/* $period = "28 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate); */
						break;
					case 12; // Ripeti solo questa singola Attività ogni 15' per 2 ore
						$period = "15 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "30 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "45 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "60 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "75 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "90 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "105 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "120 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						/* $period = "135 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "150 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "165 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "180 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "195 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "210 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "225 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "240 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate); */
						break;
					case 13; // Ripeti solo questa singola Attività ogni 30' per 2 ore
						$period = "30 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "60 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "90 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "120 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						/* $period = "150 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "180 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "210 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "240 minutes";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate); */
						break;
					case 14; // Ripeti solo questa singola Attività ogni ora per 2 ore
						$period = "1 hour";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						/* $period = "2 hours";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "3 hours";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE rowid = '".$_GET['RowID']."'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate); */
						break;
					case 15; // Ripeti tutte le Attività in questi Evento-Data per 2 giorni
						$period = "1 day";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "2 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						break;
					case 16; // Ripeti tutte le Attività in questi Evento-Data per 4 giorni
						$period = "1 day";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "2 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "3 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "4 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						/* $period = "5 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "6 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate);
						$period = "7 days";
						$sql_duplicate = "CREATE TEMPORARY TABLE tmp AS SELECT * FROM ap_attivita WHERE idEvento = '".$_GET['idEvento']."' AND substr(DataOraInizio,1,10) LIKE '".substr($_GET['DataOraInizio'],0,10)."%'; UPDATE tmp SET DataOraInizio = substr(datetime(DataOraInizio,'".$period."'),1,16), DataOraFine = substr(datetime(DataOraFine,'".$period."'),1,16); INSERT INTO ap_attivita SELECT * FROM tmp; DROP TABLE tmp;";
						$dbs->exec($sql_duplicate); */
						break;
					case 17; // Ripeti tutte le Attività in questi Evento-Data per 4 settimane
						/* ...mancante... */
						break;
				}
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF']).($duplicate='0'?"?action=update&RowID=".$_GET['RowID']:"")."'</script>";
				exit;
			}
			/* ===== RETRIEVE-ONE */
			if (isset($_GET['action']) && ($_GET['action']=="retrieve" || $_GET['action']=="update") && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$qry = $dbs->query($sql_retrieve1);
				$row = $qry->fetchArray();
			}
			/* ===== UPDATE */
			if (isset($_GET['action']) && $_GET['action']=="update-save") {
				$dbs->exec($sql_update);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DELETE */
			if (isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$dbs->exec($sql_delete);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			$dbs->close(); ?>
			<div class="container-fluid">
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>">
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action'])) { ?>
							<?php // ################################################################################ ?>

							<style>tbody, td, tfoot, th, thead, tr { border:none !important; }</style>
							<table border="1" style="width:100%;" class="table table-sm table-responsive table-striped align-middle table-hover" id="sortTable" data-lang="it">
							<!-- table border="1" style="width:100%;" class="display table table-sm table-responsive table-striped align-middle table-hover" id="sortTable" data-lang="it" -->
								<thead><?php // ##### CPS, Fields ?>
									<tr>
										<th class="col-0">P.</th>
										<th class="col-4">Evento</td>
										<th class="col-3">Inizio</td>
										<th class="col-3">Fine</td>
										<th class="col-1">Luogo</td>
										<th class="col-1">Posti</td>
									</tr>
								</thead>
								<tbody>
									<?php
									include 'ap_sqlite.php';
									$qry = $dbs->query($sql_retrieve2);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										echo "<tr class='table-row text-dark text-link'>
											<td class='col-0'>".$row['Crea']."</td>
											<td class='col-4'>".$row['Evento'].", ".$row['Note']."</td>
											<td class='col-3'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['DataOraInizio']."</a></td>
											<td class='col-3'>".$row['DataOraFine']."</td>
											<td class='col-1'>".$row['Luogo']." (".$row['LuogoPosti'].")</td>
											<td class='col-1'>".$row['Posti']."</td>
										</tr>";
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>
								$('#sortTable').dataTable( {
									stateSave: true,
									"order": [[ 2, "asc" ]], 
									"lengthMenu": [ 10, 25, 50, 100 ], 
									"language": { "decimal": ",", "emptyTable": "Nessun dato disponibile.", "info": "Righe da _START_ a _END_ di _TOTAL_ totali.", "infoEmpty": "Elenco da 0 a 0 di 0 in totale.", "infoFiltered": "(filtro su _MAX_ righe)", "infoPostFix": "", "thousands": ".", "lengthMenu": "Elenca _MENU_ righe", "loadingRecords": "Lettura...", "processing": "Ricerca...", "search": "Cerca:", "zeroRecords": "Nessuna informazione disponibile.", "paginate": { "first": "Primo", "last": "ULtimo", "next": "Succ.", "previous": "Prec." }, "aria": { "sortAscending": ": Ordine crescente", "sortDescending": ": Ordine decrescente" } } 
									/*
									i = 1;
									$('#sortTable thead th').each( function ( i ) {
										var select = $('<select><option value="Scegli Evento..."></option></select>')
											.appendTo( $(this).empty() )
											.on( 'change', function () {
												table.column( i )
													.search( $(this).val() )
													.draw();
											} );
										table.column( i ).data().unique().sort().each( function ( d, j ) {
											select.append( '<option value="'+d+'">'+d+'</option>' )
										} );
									};
									*/
								} );
								/*
								var table = $('#sortTable').DataTable();
								$('#sortTable tbody').on( 'click', 'tr', function () {
									$(this).toggleClass('selected');
								} );
								$('#button').click( function () {
									alert( table.rows('.selected').data().length +' row(s) selected' );
								} );
								*/
							</script>
							<?php // ----- Create ?>
							<input type="hidden" name="action" value="create"><button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i> Aggiungi</button>
							<?php
						/* ===== CREATE-UPDATE-DELETE ===== */
						} else { ?>
							<script src="./jquery/jquery.min.js"></script>
							<link rel="stylesheet" type="text/css" href="./jquery/jquery.datetimepicker.min.css"/>
							<script src="./jquery/jquery.datetimepicker.js"></script>

							<?php // ##### CPS, idEvento x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="idEvento">Evento</label>
								<div class="col-sm-10">
									<select class="form-control" id="idEvento" name="idEvento" <?=($_GET['action']!='retrieve'?'':'disabled')?> required>
										<option value='0'>Scegli...</option>
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT rowid, * FROM ap_eventi WHERE idTipo < '3' ORDER BY Descrizione ASC"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['rowid']."'".
												(($_GET['action']!='create' && $row['idEvento']==$row2['rowid'])?" selected":"").">".
												$row2['Descrizione'].', '.$row2['Note']."</option>";
											}
											$dbs2->close();
										?>
									</select>
								</div>
							</div>

							<?php // ##### CPS, DataOraInizio x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-2 col-form-label" for="DataOraInizio">Inizio</label><div class="col-sm-10">
								<input class="form-control" type="text" id="DataOraInizio" name="DataOraInizio" 
									<?=($_GET['action']!="create"?"value='".$row['DataOraInizio']."'":"").($_GET['action']=="retrieve"?' disabled':'')?> required></div>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									$('#DataOraInizio').datetimepicker({
										lang:'it',
										i18n:{it:{months:['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugo','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre',],
											dayOfWeek:["Dom", "Lun", "Mar", "Mer","Gio", "Ven", "Sab",]}},
										format:'Y-m-d H:i',
									});
								});
							</script>

							<?php // ##### CPS, DataOraFine x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-2 col-form-label" for="DataOraFine">Fine</label><div class="col-sm-10">
								<input class="form-control" type="text" id="DataOraFine" name="DataOraFine" 
									<?=($_GET['action']!="create"?"value='".$row['DataOraFine']."'":"").($_GET['action']=="retrieve"?' disabled':'')?> required></div>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									$('#DataOraFine').datetimepicker({
										lang:'it',
										i18n:{it:{months:['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugo','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre',],
											dayOfWeek:["Dom", "Lun", "Mar", "Mer","Gio", "Ven", "Sab",]}},
										format:'Y-m-d H:i',
									});
								});
							</script>

							<?php // ##### CPS, idLuogo x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="idEvento">Luogo</label>
								<div class="col-sm-10">
									<select class="form-control" id="idLuogo" name="idLuogo" <?=($_GET['action']!='retrieve'?'':'disabled')?> required>
										<option value='0'>Scegli...</option>
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT rowid, * FROM ap_luoghi"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											$PostiDefault = 0;
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['rowid']."'".
												(($_GET['action']!='create' && $row['idLuogo']==$row2['rowid'])?" selected":"").">".
												$row2['Descrizione'].' ('.$row2['Posti'].")</option>";
												$PostiDefault=(($_GET['action']!='create' && $row['idLuogo']==$row2['rowid'])?$row2['Posti']:$PostiDefault);
											}
											$dbs2->close();
										?>
									</select>
								</div>
							</div>
							<?php // ##### CPS, Posti x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Posti">Posti</label>
								<div class="col-sm-10">
									<input class="form-control" type="number" id="Posti" name="Posti" min="0" max="999999" step="1"
										<?=($_GET['action']!="create"?"value='".$row['Posti']."'":"value='".$PostiDefault."'").($_GET['action']=="retrieve"?" disabled":"")?>>
								</div>
							</div>

							<?php // ################################################################################ ?>
							<p>&nbsp;</p>

							<?php   // ----- SUBMIT, create-save
							if ($_GET['action']=='create') { ?>
								<input type="hidden" name="action" value="create-save"><button class="btn btn-success" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, duplicate-save
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<div class="btn-group">
									<input type="hidden" name="RowID" value="<?=$row['rowid']?>">
									<input type="hidden" name="action" value="duplicate-save">
									<input type="hidden" name="idEvento" value="<?=$row['idEvento']?>">
									<input type="hidden" name="DataOraInizio" value="<?=$row['DataOraInizio']?>">
									<select id="duplicate" name="duplicate" style="background-color:#21883899;text-align:left;" class="btn btn-dropdown" data-toggle="dropdown">
										<option value='0'>Duplica solamente questa singola Attività</option>
										<option value='1'>Duplica solamente questa singola Attività fra 15 minuti</option>
										<option value='2'>Duplica solamente questa singola Attività fra 30 minuti</option>
										<option value='3'>Duplica solamente questa singola Attività fra 1 ora</option>
										<option value='4'>Duplica solamente questa singola Attività fra 1 giorno</option>
										<!-- option value='5'>Duplica solamente questa singola Attività fra 2 giorni</option --><!-- eliminare, 2021-04-23 -->
										<option value='6'>Duplica solamente questa singola Attività fra 1 settimana</option>
										<!-- option value='7'>Duplica solamente questa singola Attività fra 4 settimane</option --><!-- eliminare -->
										<option value='8'>Duplica tutte le Attività in questi Evento-Data fra 1 giorno</option>
										<option value='9'>Duplica tutte le Attività in questi Evento-Data fra 3 giorni</option><!-- diventano 3gg. -->
										<option value='10'>Duplica tutte le Attività in questi Evento-Data fra 1 settimana</option>
										<!-- option value='11'>Duplica tutte le Attività in questi Evento-Data fra 4 settimane</option --><!-- eliminare -->
										<option value='12'>Ripeti solo questa singola Attività ogni 15' per 2 ore</option><!-- diventa 2h -->
										<option value='13'>Ripeti solo questa singola Attività ogni 30' per 2 ore</option><!-- diventa 2h -->
										<option value='14'>Ripeti solo questa singola Attività ogni ora per 2 ore</option><!-- diventa 2h -->
										<option value='15'>Ripeti tutte le Attività in questi Evento-Data per 2 giorni</option>
										<option value='16'>Ripeti tutte le Attività in questi Evento-Data per 4 giorni</option><!-- diventa 5gg -->
										<!-- option value='17'>Ripeti tutte le Attività in questi Evento-Data per 4 settimane</option -->
									</select>&nbsp;&nbsp;
									<button class="btn btn-success" type="submit"><i class="fa fa-plus-square"></i> Duplica/Ripeti</button>
								</div>
							<?php } // ----- SUBMIT, update-save
							if ($_GET['action']=='update') { ?>
								<input type="hidden" name="RowID" value="<?=$row['rowid']?>"><input type="hidden" name="action" value="update-save"><button class="btn btn-primary" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=update&RowID=<?=$row['rowid']?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Modifica</a>
							<?php   // ----- SUBMIT, Alert delete ?>
								<a href="javascript: confirm_delete(<?=$row['rowid']?>);" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Elimina</a>
								<script type="text/javascript">
								function confirm_delete( rid, uid ) {
									if (confirm('Eliminare la riga '+ rid + ' ?')) {
										window.location.href = '<?=htmlentities($_SERVER['PHP_SELF'])?>?action=delete&RowID=' + rid;
									}
								}
								</script>
							<?php } // ----- Back ?>
							<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i>&nbsp;Ritorna</a>
						<?php } ?>
					</div>
				</form>
			</div>
	<?php include 'ap_footer.php'; ?>
