<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage in "database.sqlite" the data in table "ap_eventi":
 * Field					Type		!N	Value	Key
 * ------------------------+-----------+---+-------+----
 * Descrizione				TEXT		No	None	No
 * Note						TEXT		No	None	No
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - Action = Create, Retrieve (One-RowID), Update (RowID) or Delete (RowID)
 * then comes two form:
 * - Retrieve All, when no action (DataTable: responsive, search, sort, pagination)
 * - Retrieve One-RowID for Create, Update, Delete actions with details, validation
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}

		// ################################################################################
		$scp_name ="eventi";
		$scp_Table="ap_".$scp_name;
		$sql_create = "INSERT INTO $scp_Table (Descrizione, Note, idTipo) VALUES ('".$_GET['Descrizione']."', '".$_GET['Note']."', '".$_GET['idTipo']."')";
		$sql_duplicate = "INSERT INTO $scp_Table SELECT * FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
		$sql_retrieve1 = "SELECT rowid, * FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
		$sql_update = "UPDATE $scp_Table SET Descrizione = '".$_GET['Descrizione']."', Note = '".$_GET['Note']."', idTipo = '".$_GET['idTipo']."' WHERE rowid = '".$_GET['RowID']."'";
		$sql_delete = "DELETE FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
		$sql_retrieve2 = "SELECT rowid, * FROM $scp_Table WHERE idTipo < '3' ORDER BY Descrizione";
		// ################################################################################

		include 'ap_header.php'; ?>
		<title><?=ucfirst($scp_name)?></title>
		<script>
			var a = document.getElementById("menu-<?=$ap_Name?>");
			a.classList.add("active");  
		</script>

	<?php include 'ap_menu.php'; ?>

			<h2 class="alert alert-secondary"><?=ucfirst($scp_name)?></h2>
			<?php
			include 'ap_sqlite.php';
			/* ===== CREATE */
			if (isset($_GET['action']) && $_GET['action']=="create-save") {
				$dbs->exec($sql_create);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DUPLICATE */
			if (isset($_GET['action']) && $_GET['action']=="duplicate-save") {
				$dbs->exec($sql_duplicate);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."?action=update&RowID=".$_GET['RowID']."'</script>";
				exit;
			}
			/* ===== RETRIEVE-ONE */
			if (isset($_GET['action']) && ($_GET['action']=="retrieve" || $_GET['action']=="update") && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$qry = $dbs->query($sql_retrieve1);
				$row = $qry->fetchArray();
			}
			/* ===== UPDATE */
			if (isset($_GET['action']) && $_GET['action']=="update-save") {
				$dbs->exec($sql_update);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DELETE */
			if (isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$dbs->exec($sql_delete);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			$dbs->close(); ?>
			<div class="container-fluid">
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>">
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action'])) { ?>
							<?php // ################################################################################ ?>

							<table border="1" style="width:100%;" class="table table-sm table-responsive table-striped align-middle table-hover" id="sortTable" data-lang="it">
								<thead><?php // ##### CPS, Fields ?>
									<td class="col-sm-6">Descrizione</td>
									<td class="col-sm-5">Note</td>
									<td class="col-sm-1">Tipo</td>
								</thead>
								<tbody>
									<?php
									include 'ap_sqlite.php';
									$qry = $dbs->query($sql_retrieve2);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										echo "<tr class='table-row text-dark text-link'>
											<td class='col-sm-6'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Descrizione']."</a></td>
											<td class='col-sm-5'>".$row['Note']."</td>
											<td class='col-sm-1'>".$row['idTipo']."</td>
										</tr>";
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>
								$('#sortTable').dataTable({
									stateSave: true,
									"lengthMenu": [15, 35, 75, 150],
									"language": {"decimal": ",", "emptyTable": "Nessun dato disponibile.", "info": "Righe da _START_ a _END_ di _TOTAL_ totali.", "infoEmpty": "Elenco da 0 a 0 di 0 in totale.", "infoFiltered": "(filtro su _MAX_ righe)", "infoPostFix": "", "thousands": ".", "lengthMenu": "Elenca _MENU_ righe", "loadingRecords": "Lettura...", "processing": "Ricerca...", "search": "Cerca:", "zeroRecords": "Nessuna informazione disponibile.", "paginate": {"first": "Primo", "last": "ULtimo", "next": "Succ.", "previous": "Prec."}, "aria": {"sortAscending": ": Ordine crescente", "sortDescending": ": Ordine decrescente"}} });</script>
							<?php // ----- Create ?>
							<input type="hidden" name="action" value="create"><button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i> Aggiungi</button>
							<?php
						/* ===== CREATE-UPDATE-DELETE ===== */
						} else { ?>
							<?php // ##### CPS, Descrizione x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-6 col-form-label" for="Descrizione">Descrizione</label><div class="col-sm-10">
								<input type="text" class="form-control" id="Descrizione" name="Descrizione" 
									<?=($_GET['action']!="create"?"value='".$row['Descrizione']."'":'').($_GET['action']=="retrieve"?' disabled':'')?> required></div>
							</div>
							<?php // ##### CPS, Note x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-6 col-form-label" for="Note">Note</label><div class="col-sm-10">
								<input class="form-control" type="text" id="Note" name="Note" 
									<?=($_GET['action']!="create"?"value='".$row['Note']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>></div>
							</div>
							<?php // ##### CPS, idTipo x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-6 col-form-label" for="Note">Tipo di Evento...</label><div class="col-sm-10">
								<select class="form-control" id="idTipo" name="idTipo" <?=($_GET['action']!='retrieve'?'':'disabled')?> required>
									<option value="0">Scegli...</option>
									<option value="1" <?=((($_GET['action']!='create' && $row['idTipo']=='1') || ($_GET['action']=='create'))?" selected":"")?>>1) Eventi con vincoli: Attività e date/orari (da calendario: prenotazioni)</option>";
									<option value="2" <?=(($_GET['action']!='create' && $row['idTipo']=='2')?" selected":"")?>>2) Eventi con vincolo: Luoghi (Attività, date/orari liberi: feste di compleanno)</option>";
									<option value="3" <?=(($_GET['action']!='create' && $row['idTipo']=='3')?" selected":"")?>>3) Eventi con vincolo: disponibilità (Attività, date/orari liberi: giochi)</option>";
								</select>
							</div>

							<?php // ################################################################################ ?>
							<p>&nbsp;</p>
							<?php   // ----- SUBMIT, create-save
							if ($_GET['action']=='create') { ?>
								<input type="hidden" name="action" value="create-save"><button class="btn btn-success" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, duplicate-save
							if ($_GET['action']!='update' && $_GET['action']!='create' && isset($_SESSION['Email'])) { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=duplicate-save&RowID=<?=$row['rowid']?>" class="btn btn-success"><i class="fa fa-plus-square"></i>&nbsp;Duplica</a>
							<?php } // ----- SUBMIT, update-save
							if ($_GET['action']=='update') { ?>
								<input type="hidden" name="RowID" value="<?=$row['rowid']?>"><input type="hidden" name="action" value="update-save"><button class="btn btn-primary" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=update&RowID=<?=$row['rowid']?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Modifica</a>
							<?php   // ----- SUBMIT, Alert delete ?>
								<a href="javascript: confirm_delete(<?=$row['rowid']?>);" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Elimina</a>
								<script type="text/javascript">
								function confirm_delete( rid, uid ) {
									if (confirm('Eliminare la riga '+ rid + ' ?')) {
										window.location.href = '<?=htmlentities($_SERVER['PHP_SELF'])?>?action=delete&RowID=' + rid;
									}
								}
								</script>
							<?php } // ----- Back ?>
							<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i>&nbsp;Ritorna</a>
						<?php } ?>
					</div>
				</form>
			</div>

	<?php include 'ap_footer.php'; ?>
