<?php
/* ================================================================================
 * Web App named "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * Include this script at the top of every PHP script for JS, CSS, Fonts, ...
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="./font/css/all.css" rel="stylesheet"><?php /* 2021-04-25, CPS = Commented CSS rules with: \f0dc, \f0dd, \f0de */ ?>
		<?php // Bootstrap, bundle 5 ?>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
		<?php // Bootstrap, jQueryPopper ?>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" /><?php // Check B4-5? ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		<?php // Tables, dataSortSearchPagination ?>
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" />
		<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		<?php // Password, toggle ?>
		<link rel="stylesheet" href="./css/show-password-toggle.css" />
		<script src="./js/show-password-toggle.min.js"></script>

		<style>
			table a { color: #157bbd !important; }
			.btn { padding: 0.4rem !important; margin-top: 0.2rem; }
			.dataTable a { font-weight: bold; }
			.text-link { text-decoration: none !important; font-weight: bold !important; }
			@media (max-width: 768px) {
				body, .container-fluid { padding-left: 0px; padding-right: 0px; margin-left: 0px; margin-right: 0px; }
				.row { margin-left: -8px; margin-right: -8px; }
				.col-sm-12 { padding: 0; }
			}
			.bg-dark { background-color:#39664b !important; }
			#back-image { width:100%; min-height:64px; background-image:url('./img/ambienteparco-sfondo.jpg'); background-repeat:repeat; background-size:contain; }
			a.navbar-brand { font-weight:bold; color:#abc93d !important;}
			h2.alert.alert-secondary { background-color:#739f2f !important; margin:0 -20px !important; border:none; border-bottom:4px solid #6c4c1b; margin-bottom:4px; font-size:1.75em; padding:0.25rem 1.25rem !important; }

			#sortTable_wrapper table.dataTable thead .sorting_asc, #sortTable_wrapper table.dataTable thead .sorting_desc { background-image: none !important; }
			#sortTable_wrapper div.row:nth-child(1) {
				background-color: #39664b;
				padding: 10px 0 0 0;
				margin: 30px -30px 0 -30px;
				color: #fff;
			}

		</style>
		<?php // 2021-04-24, CPS. Force User Agent specific CSS rules (Apple/Google)
		$u_agent=$_SERVER['HTTP_USER_AGENT'];
		// echo "<hr>".$u_agent."<hr>";
		if (preg_match('/Chrom/i',$u_agent) || preg_match('/Safari/i',$u_agent)) { ?>
			<style>
				td.col-0 { max-width: 2% !important; }
				.col, .col-auto, .col-lg-auto, .col-md-auto, .col-sm-auto, .col-xl-auto,
				.col-1, .col-2, .col-3, .col-4, .col-5,
				.col-lg, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5,
				.col-md, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5,
				.col-sm, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5,
				.col-xl, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5 { width: 10% !important; }
				.col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12,
				.col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12,
				.col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12,
				.col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12,
				.col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12 { width: auto !important; }
			</style>
		<?php } ?>
