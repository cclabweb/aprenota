<?php
/* ================================================================================
 * Web App named "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * Include this script after the starting HTML <head> for top-sticky menu
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
	/*if (!isset($_SESSION['Email'])) {
		header("Location: ".htmlentities($_SERVER['PHP_SELF']));
		exit;
	}*/
	// --- CHANGELOGS --- Previous changes, before 2021-02-06 23.50, were registered locally...
	$ver = "1.0207.0915"; // Started duplication of an entire day for a single Activity...
	$ver = "1.0207.0955"; // Select Activities from a single day, not starting from that day...
	$ver = "1.0207.1330"; // Confirm reservation to the indicated Email, for Guest or Operators
	$ver = "1.0207.1445"; // Send Emails to both Guest and, using bcc, to info@AmbienteParco.it
	$ver = "1.0209.1615"; // Export consolidated data from table 'ap_prenotazioni' using a View
	$ver = "1.0209.1925"; // Duplication or Replication of Activities based on Events and Dates
	$ver = "1.0215.1235"; // Add a unique hash in confirmation email, to cancel the reservation
	$ver = "1.0222.1220"; // Add required consent to GDPR and correct order "YMDHMS" in booking
	#ver = "1.0222.1630"; // Add two levels in Activities, as in Bookings, to simplify the list
	$ver = "1.0223.1630"; // Add permanent filters on datatables to remember searches on tables
	#ver = "1.0224.1630"; // Add records selection and Delete button to make multiple deletions
	$ver = "1.0328.0030"; // Add colored borders and backgrounds in days with activities booked
	$ver = "1.0402.0920"; // Add calendar table and actions of activities available for booking
	$ver = "1.0419.2100"; // More calendar functions, filters, alerts, for operators and guests
	$ver = "1.0424.1645"; // Add CAP+Note in DB, CSS for UA, changed Duplicate, details in mail
	$ver = "1.0508.2045"; // Add ap_tipi.php and ap_giochi.php, rename ap_tipi>idTipo = Livello
	$ver = "1.0510.1850"; // New type of booking with one constraint (only availability, games)
	#ver = "1.mmdd.hhmm"; // Add exportations in .csv format of bookings, events and activities
	#ver = "1.mmdd.hhmm"; // Add graphs on webapp guest/users access/login on UA, IP, time, ...
	/*
> 2) Nel Database sulla tabella luoghi, si può inserire un luogo che rappresenta l'insieme di tutti i luoghi e che una volta inserito esclude tutti gli altri?, esempio: il giorno x abbiamo un evento y che occupa tutto il parco, posso inserire l'evento y nel giorno x nel luogo: "TUTTO IL PARCO" in modo che questo mi blocchi tutti gli altri luoghi?
2) dovrò 'dividere' la compilazione in due: dpo aver indicato Evento/Attività e Data/Orario la webapp potrà mostrare solo i luoghi disponibili, come richiesto; incomincio a ragionarci e ci riaggiorniamo per confronto e conferme.
-- 
> 3)Aggiungiamo (nella zona prenotazioni, lato operatore già autenticato) sulla visione giornaliera, una riga in cima a tutte le altre dove si può inserire un alert a mo' di nota generale? Esempio: il gioro x abbiamo i sub che puliscono l'acquario, e si aggiunge: PULIZIA SUB come nota generale alla giornata?
3) Uhm, a questo punto aggiungerei una nuova tabella di "Avvisi" (che un domani potremo anche distinguere in base al ruolo: interni, utenti registrati, ospiti) basata su Data, Orario (opzionale), Ruolo (visibilità), Note, Livello (urgenza).
Anche qui inizio a preparare la tabella e ragionarci, fatemi sapere se va bene.
-- 
> 5) Possiamo aggiungere una pagina "operatori" che ci fa vedere l'operatore x in che giorni e per che cosa è impegnato?
5) Ottima! Anche per questa provvedo e vi aggiorno (*spero entrambe per domani). 
* Operatore, DataOraInizio, DataOraFine, Evento, Luogo, Presenze
* 
	*/
	?>
	</head>
	<body>
		<header class="sticky-top">

			<!-- NAVBAR -->
			<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark" style="<?=(strpos($_SERVER['PHP_SELF'],'_dev')?'background-color:#900 !important;':( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'background-color:#157abb !important;':''))?>">
				<div class="container-fluid">
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="#"><img src="./img/ambienteparco-<?=(strpos($_SERVER['PHP_SELF'],'_dev')?'demo':'text')?>.png" style="margin-left:-20px;max-width:180px;"></a> <?php // https://www.ocdl.it/wordpress/ap-01/wp-content/webapp/cal/index.php ?>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav me-auto mb-2 mb-lg-0">
							<?php if (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) { ?>
								<li class="nav-item dropdown" style="<?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'display:none;')?>">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="font-weight:bold;">
										Eventi
									</a>
									<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
										<li><a class="dropdown-item" href="ap_prenotazioni.php" style="font-weight:bold;">Prenotazioni</a></li>
										<li><hr class="dropdown-divider"></li>
										<li><a class="dropdown-item" href="ap_attivita.php" id="menu-attivita">Calendario</a></li>
										<li><hr class="dropdown-divider"></li>
										<li><a class="dropdown-item" href="ap_eventi.php" id="menu-eventi">Eventi</a></li>
										<li><a class="dropdown-item" href="ap_luoghi.php" id="menu-luoghi">Luoghi</a></li>
										<li><hr class="dropdown-divider"></li>
										<li><a class="dropdown-item" href="ap_oprenotati.php">Operatori prenotati</a></li> <?php // https://www.ocdl.it/wordpress/ap-01/wp-content/webapp/cal/index.php ?>
										<?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<0) ?'<li><a class="dropdown-item" href="ap_presenze.php">Presenze</a></li>':'')?> <?php // https://www.ocdl.it/wordpress/ap-02/wp-content/webapp/ ?>
									</ul>
								</li>
								<li class="nav-item dropdown" style="<?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'display:none;')?>">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="font-weight:bold;">
										Kit/Giochi
									</a>
									<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
										<li><a class="dropdown-item" href="ap_prenotazioni-3.php" style="font-weight:bold;">Prenotazioni</a></li>
										<li><hr class="dropdown-divider"></li>
										<li><a class="dropdown-item" href="ap_attivita-3.php" id="menu-attivita">Disponibilit&agrave;</a></li>
										<li><hr class="dropdown-divider"></li>
										<li><a class="dropdown-item" href="ap_eventi-3.php" id="menu-eventi">Kit/Giochi</a></li>
									</ul>
								</li>
								<li class="nav-item dropdown" style="<?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'display:none;')?>">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
										Persone
									</a>
									<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
										<li><a class="dropdown-item" href="ap_utenti.php" id="menu-utenti">Persone</a></li>
										<li><a class="dropdown-item" href="ap_gruppi.php" id="menu-gruppi">Gruppi</a></li>
									</ul>
								</li>
							<?php } else { ?>
								<li class="nav-item dropdown">
									<li><a class="nav-link" href="ap_prenotazioni.php" style="font-weight:bold;">Eventi</a></li>
									<li><a class="nav-link" href="ap_prenotazioni-3.php" style="font-weight:bold;">Kit/Giochi</a></li>
								</li>
							<?php } ?>
							<li class="nav-item dropdown">
								<?php if (isset($_SESSION['Email']) && !empty($_SESSION['Email'])) { ?>
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
										Opzioni
									</a>
									<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
										<?php if (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) { ?>
											<li><a class="dropdown-item" href="ap_profilo.php">Profilo</a></li>
											<li><a class="dropdown-item <?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'disabled')?>" href="ap_operatori.php" id="menu-operatori">Operatori</a></li>
											<li><hr class="dropdown-divider"></li>
											<li><a class="dropdown-item <?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'disabled')?>" href="ap_ruoli.php">Ruoli</a></li>
											<li><a class="dropdown-item <?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'disabled')?>" href="#">Incarichi</a></li>
											<li><hr class="dropdown-divider"></li>
											<li><a class="dropdown-item <?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'disabled')?>" href="ap_tipi.php">Tipi</a></li>
											<li><a class="dropdown-item <?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ?'':'disabled')?>" href="#">Vincoli</a></li>
											<li><hr class="dropdown-divider"></li>
											<?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<2) ? '<li><a class="dropdown-item" href="#" style="color:#999;">Ver. '.$ver.'</a></li>' :'')?>
											<?=( (isset($_SESSION['Ruolo']) && $_SESSION['Ruolo']<1) ? '<li><a class="dropdown-item" href="phpliteadmin/phpliteadmin.php" target="_blank">Database</a></li>' :'')?>
										<?php } ?>
										<li><a class="dropdown-item" href="index.php?logout">Esci</a></li>
									</ul>
								<?php } else { ?>
									<li><a class="nav-link" href="index.php">Accedi</a></li>
								<?php } ?>
							</li>
						</ul>
					</div>
				</div>
			</nav>

		</header>
		<div id="back-image"></div>
		<div class="container-fluid">
