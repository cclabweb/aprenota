<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage in "database.sqlite" the data in view "ap_oprenotati":
 * Field					Type		!N	Value	Key
 * ------------------------+-----------+---+-------+----
 * idOperatore				INTEGER		No	None	No
 * DataOraInizio			TEXT		No	None	No
 * DataOraFine				TEXT		No	None	No
 * idEvento					INTEGER		No	None	No
 * idLuogo					INTEGER		No	None	No
 * Presenze					INTEGER		No	None	No
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - View only as a report...
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}

		// ################################################################################ CPS. Title-Table-Name and 5 SQL Queries (CR12UD)
		$scp_name ="Operatori prenotati";
		$scp_table="OperatoriPrenotati";
		$sql_retrieve = "SELECT rowid, * FROM $scp_table";
		// ################################################################################

		include 'ap_header.php'; ?>
		<title><?=ucfirst($scp_name)?></title>
		<script>
			var a = document.getElementById("menu-<?=$scp_name?>");
			a.classList.add("active");  
		</script>

	<?php include 'ap_menu.php'; ?>

			<h2 class="alert alert-secondary"><?=ucfirst($scp_name)?></h2>
			<div class="container-fluid">
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>">
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action'])) { ?>
							<?php // ################################################################################ ?>

							<table border="1" class="table table-sm table-responsive table-striped align-middle table-hover" class="table" id="sortTable" data-lang="it">
								<thead><?php // ##### CPS, Fields ?>
									<td class="col-2">Operatore</td>
									<td class="col-2">Data/Ora, inizio</td>
									<td class="col-2">Termine</td>
									<td class="col-4">Evento</td>
									<td class="col-1">Luogo</td>
									<td class="col-1">Presenze</td>
								</thead>
								<tbody>
									<?php
									include 'ap_sqlite.php';
									$qry = $dbs->query($sql_retrieve);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										echo "<tr class='table-row text-dark text-link'>
											<td class='col-2'>".$row['Operatore']."</td>
											<td class='col-2'>".(strtotime($row['DataOraInizio'])>=strtotime(date("Y-m-d h:i:s"))?"<a href='ap_prenotazioni.php?action=update&RowID=".$row['rowid']."'>":"").$row['DataOraInizio'].(strtotime($row['DataOraInizio'])>=strtotime(date("Y-m-d h:i:s"))?"</a>":"")."</td>
											<td class='col-2'>".$row['DataOraFine']."</td>
											<td class='col-4'>".$row['Evento']."</td>
											<td class='col-1'>".$row['Luogo']."</td>
											<td class='col-1'>".$row['Presenze']."</td>
										</tr>";
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>$('#sortTable').dataTable( { "lengthMenu": [ 15, 35, 75, 150 ], "language": { "decimal": ",", "emptyTable": "Nessun dato disponibile.", "info": "Righe da _START_ a _END_ di _TOTAL_ totali.", "infoEmpty": "Elenco da 0 a 0 di 0 in totale.", "infoFiltered": "(filtro su _MAX_ righe)", "infoPostFix": "", "thousands": ".", "lengthMenu": "Elenca _MENU_ righe", "loadingRecords": "Lettura...", "processing": "Ricerca...", "search": "Cerca:", "zeroRecords": "Nessuna informazione disponibile.", "paginate": { "first": "Primo", "last": "ULtimo", "next": "Succ.", "previous": "Prec." }, "aria": { "sortAscending": ": Ordine crescente", "sortDescending": ": Ordine decrescente" } } } );</script>
						<?php } ?>
					</div>
				</form>
			</div>

	<?php include 'ap_footer.php'; ?>
