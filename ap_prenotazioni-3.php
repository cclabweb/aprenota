<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage "database.sqlite" data in table: "ap_prenotazioni" (3)
 * Field					Type		!N	Value	Key
 * ------------------------+-----------+---+-------+----
 * idEvento					INTEGER		No	None	No	<=	rowid[ap_eventi]
 * idAttivita				INTEGER		No	None	No	<=	rowid[ap_attivita]
 * idGruppo					INTEGER		No	None	No	<=	rowid[ap_gruppi]
 * idUtente					INTEGER		No	None	No	<=	rowid[ap_utenti]
 * DataOraPresenza			DATETIME	No	None	No
 * Presenze					INTEGER		No	None	No
 * Importo					INTEGER		No	None	No
 * idOperatore				INTEGER		No	None	No	<=	rowid[ap_operatori]
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - Action = Create, Retrieve (One-RowID), Update (RowID) or Delete (RowID)
 * then comes two form:
 * - Retrieve All, when no action (DataTable: responsive, search, sort, pagination)
 * - Retrieve One-RowID for Create, Update, Delete actions with details, validation
 * + added state save to search function so filters will be permanently
 * ~ "Evento, Note" only if Note is not empty in every Query
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021 - ocdl.it/pap - Released 2020-09-26 - Updated 2021-04-19 19.05
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		/* CPS, disabled for guests...
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}*/

		// ################################################################################ CPS. Title-Table-Name and 5 SQL Queries (CR12UD)
		$scp_Name ="Kit/Giochi disponibili";
		$scp_Table="ap_prenotazioni"; // .$scp_Name;
		$sql_create = "INSERT INTO $scp_Table (idEvento, idAttivita, idGruppo, idUtente, DataOraPresenza, Presenze, Importo, idOperatore, Cognome, Nome, Email, Telefono, CAP, Note) 
					VALUES ('".$_GET['idEvento']."', '".$_GET['idAttivita']."', '".$_GET['idGruppo']."', '".$_GET['idUtente']."', '".$_GET['DataOraPresenza']."', '".$_GET['Presenze']."', '".$_GET['Importo']."', '".$_GET['idOperatore']."', 
					'".$_GET['Cognome']."', '".$_GET['Nome']."', '".$_GET['Email']."', '".$_GET['Telefono']."', '".$_GET['CAP']."', '".$_GET['Note']."')";
		$sql_duplicate = "INSERT INTO $scp_Table SELECT * FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'; 
					UPDATE $scp_Table SET idGruppo = '', idUtente = '', DataOraPresenza = '', Presenze = '', Importo = '', Cognome = '', Nome = '', Email = '', Telefono = '', CAP = '', Note = ''
					WHERE rowid = '".$_GET['RowID']."'";
	$sql_retrieve1 = "SELECT p.rowid, p.idEvento, e.Descrizione || CASE WHEN e.Note = '' THEN ', ' || e.Note END AS Evento, 
					p.idAttivita, a.idEvento, a.DataOraInizio, a.DataOraFine, l.Descrizione || ' (' || a.Posti || ')' AS Luogo,
					p.idGruppo, g.Descrizione || CASE WHEN g.Note = '' THEN ', ' || g.Note END AS Gruppo, p.idUtente, u.Cognome || ' ' || u.Nome || ', ' || u.Denominazione AS Utente, 
					p.DataOraPresenza, p.Presenze, p.Importo, p.idOperatore, o.Cognome || ' ' || o.Nome AS Operatore,
					p.Cognome, p.Nome, p.Email, p.Telefono, p.CAP, p.Note, a.Posti
					FROM ap_prenotazioni AS p 
					LEFT JOIN ap_attivita AS a ON p.idAttivita = a.rowid	LEFT JOIN ap_eventi AS e ON a.idEvento = e.rowid
					LEFT JOIN ap_luoghi AS l ON a.idLuogo = l.rowid 		LEFT JOIN ap_gruppi AS g ON p.idGruppo = g.rowid 
					LEFT JOIN ap_utenti AS u ON p.idUtente = u.rowid 		LEFT JOIN ap_operatori AS o ON p.idOperatore = o.rowid 
					WHERE p.rowid = '".$_GET['RowID']."' ORDER BY a.DataOraInizio DESC";
		$sql_update = "UPDATE $scp_Table SET idEvento = '".$_GET['idEvento']."', idAttivita = '".$_GET['idAttivita']."', 
					idGruppo = '".$_GET['idGruppo']."', idUtente = '".$_GET['idUtente']."', 
					DataOraPresenza = '".$_GET['DataOraPresenza']."', Presenze = '".$_GET['Presenze']."', Importo = '".$_GET['Importo']."', idOperatore = '".$_GET['idOperatore']."', 
					Cognome = '".$_GET['Cognome']."', Nome = '".$_GET['Nome']."', Email = '".$_GET['Email']."', Telefono = '".$_GET['Telefono']."', CAP = '".$_GET['CAP']."', Note = '".$_GET['Note']."'
					WHERE rowid = '".$_GET['RowID']."'";
			   $msg = "Evento:     "."\r\n".
					"Inizio:     ".$_GET['DataOraInizio']."\r\n".
					"Termine:    ".$_GET['DataOraFine']."\r\n".
					"Nominativo: ".$_GET['Cognome']." ".$_GET['Nome']."\r\n".
					"E-Mail:     ".$_GET['Email']."\r\n".
					"Telefono:   ".$_GET['Telefono']."\r\n".
					"Luogo (p.): "."\r\n".
					"Operatore:  "."\r\n\r\n".
					"Per qualsiasi dubbio o domanda scriveteci,\r\narrivederci a presto e buona giornata.\r\n\r\nAmbienteParco\r\nTelefoni: 347 4316726 - 030361347\r\n\r\n";
				$rst = "Per annullare la prenotazione cliccare sul seguente link:\r\n".
					htmlentities(($_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])?'https':'http')."://".$_SERVER['HTTP_HOST'].
					$_SERVER['PHP_SELF'])."?action=reset&date-reset=".substr($_GET['DataOraInizio'],0,10).
					"&idp=".hash("sha256", $_GET['idEvento'].$_GET['idAttivita'].(empty($_GET['idUtente'])?$_GET['Email']:$_GET['idUtente']), false);
		$sql_delete = "DELETE FROM $scp_Table WHERE rowid = '".$_GET['RowID']."'";
	$sql_retrieve2 = "SELECT p.rowid, p.idEvento, e.Descrizione || CASE WHEN e.Note = '' THEN '' ELSE ', ' || e.Note END AS Evento, 
					p.idAttivita, a.idEvento, a.DataOraInizio, a.DataOraFine, l.Descrizione || ' (' || a.Posti || ')' AS Luogo,
					p.idGruppo, g.Descrizione || CASE WHEN g.Note = '' THEN '' ELSE ', ' || g.Note END AS Gruppo, p.idUtente, u.Cognome || ' ' || u.Nome || ', ' || u.Denominazione AS Utente, 
					p.DataOraPresenza, p.Presenze, p.Importo, p.idOperatore, o.Cognome || ' ' || o.Nome AS Operatore,
					p.Cognome, p.Nome, p.Email, p.Telefono, p.CAP, p.Note, a.Posti
					FROM ap_prenotazioni AS p 
					LEFT JOIN ap_attivita AS a ON p.idAttivita = a.rowid	LEFT JOIN ap_eventi AS e ON p.idEvento = e.rowid
					LEFT JOIN ap_luoghi AS l ON a.idLuogo = l.rowid 		LEFT JOIN ap_gruppi AS g ON p.idGruppo = g.rowid 
					LEFT JOIN ap_utenti AS u ON p.idUtente = u.rowid 		LEFT JOIN ap_operatori AS o ON p.idOperatore = o.rowid
					WHERE p.idAttivita IS NOT NULL AND e.idTipo = '3' AND p.DataOraPresenza = '' AND
					a.DataOraInizio >= '".(isset($_GET['d'])?date("Y-m-d",(int)$_GET['d']):date("Y-m-d"))."' ".
					(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?"":
						"AND a.DataOraInizio >= '".date("Y-m-d")."' ".
						"AND p.idUtente || p.DataOraPresenza || p.Presenze || p.Importo || p.Cognome || p.Nome || p.Email || p.Telefono = '' ").
					($_GET['action']=='retrieve-rows'?
						(isset($_GET['idEvento'])?" AND a.idEvento = '".$_GET['idEvento']."'":"")." AND substr(a.DataOraInizio,1,10) = '".$_GET['DataOraInizio']."' ":
						" ").
					"ORDER BY a.DataOraInizio ASC";
					// .(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?"":" AND p.idUtente || p.DataOraPresenza || p.Presenze || p.Importo || p.Cognome || p.Nome || p.Email || p.Telefono = ''");
		// ################################################################################

		include 'ap_header.php'; ?>
		<title><?=ucfirst($scp_Name)?></title>
		<script>
			var a = document.getElementById("menu-<?=$ap_Name?>");
			a.classList.add("active");  
		</script>

	<?php include 'ap_menu.php'; ?>

			<?php
			/* echo $_GET['action']."<br/>";
			echo "<b>sql_retrieve1=</b>".$sql_retrieve2."<br/>";
			echo "<b>sql_update=</b>".$sql_update."<br/>"; */
			include 'ap_sqlite.php';
			/* ===== CREATE */
			if (isset($_GET['action']) && $_GET['action']=="create-save") {
				$dbs->exec($sql_create);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DUPLICATE */
			if (isset($_GET['action']) && $_GET['action']=="duplicate-save") {
				// echo "DUPLICATE = ".$sql_update."<br>".$sql_duplicate;
				// exit;
				$dbs->exec($sql_update);
				$dbs->exec($sql_duplicate);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."?action=update&RowID=".$_GET['RowID']."'</script>";
				exit;
			}
			/* ===== RETRIEVE-ROWS */
			if (isset($_GET['action']) && $_GET['action']=="retrieve-rows" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$qry = $dbs->query($sql_retrieve2);
				$row = $qry->fetchArray();
			}
			/* ===== RETRIEVE-ONE */
			if (isset($_GET['action']) && ($_GET['action']=="retrieve" || $_GET['action']=="update") && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$qry = $dbs->query($sql_retrieve1);
				$row = $qry->fetchArray();
			}
			/* ===== UPDATE */
			if (isset($_GET['action']) && $_GET['action']=="update-save") {
				// if ($_SESSION['Ruolo']<2 && isset($_SESSION['Email'])) {
					if ($_GET['Email'] != "") {
						include 'ap_sqlite-row.php';
						$sql2 = "SELECT e.Descrizione || CASE WHEN e.Note = '' THEN '' ELSE ', ' || e.Note END AS Evento, 
								l.Descrizione || ' (' || a.Posti || ')' AS Luogo,
								g.Descrizione || CASE WHEN g.Note = '' THEN '' ELSE ', ' || g.Note END AS Gruppo,
								u.Cognome || ' ' || u.Nome || ', ' || u.Denominazione AS Utente, 
								p.DataOraPresenza, p.Presenze, p.Importo,
								p.Cognome, p.Nome, p.Email, p.Telefono, p.CAP, p.Note,
								o.Cognome || ' ' || o.Nome AS Operatore
								FROM ap_prenotazioni AS p 
								LEFT JOIN ap_attivita AS a ON p.idAttivita = a.rowid	LEFT JOIN ap_eventi AS e ON a.idEvento = e.rowid
								LEFT JOIN ap_luoghi AS l ON a.idLuogo = l.rowid 		LEFT JOIN ap_gruppi AS g ON p.idGruppo = g.rowid 
								LEFT JOIN ap_utenti AS u ON p.idUtente = u.rowid 		LEFT JOIN ap_operatori AS o ON p.idOperatore = o.rowid 
								WHERE p.rowid = '".$_GET['RowID']."'";
						$qry2 = $dbs2->query($sql2);
						$row2 = $qry2->fetchArray();
						$msg = str_replace("Evento:     ","Evento:     ".strtoupper($row2['Evento']),$msg);
						$msg = str_replace("Luogo (p.): \r\n",(empty($row2['Luogo'])?"":"Luogo (p.): ".$row2['Luogo']."\r\n"),$msg);
						$msg = str_replace("Operatore:  \r\n",(empty($row2['Note'])?"Operatore:  \r\n":"Operatore:  \r\nNote:       ".$row2['Note']),$msg);
						$msg = str_replace("Operatore:  \r\n",(empty($row2['Operatore'])?"":"Operatore:  ".$row2['Operatore']."\r\n"),$msg);
						// echo $_GET['RowID'];
						$dbs2->close();
						// echo "<hr>".$_GET['idEvento']." | ".$_GET['idAttivita'].str_replace("\r\n","<br>","<hr>Buongiorno e grazie della prenotazione!\r\n\r\n".$msg.$rst)."<hr>";
						if (strpos($_SERVER['PHP_SELF'],'_dev')) {
							mail($_GET['Email'], "AmbienteParco, prenotazione di prova", "Registrata una Prenotazione:\r\n\r\n".$msg.$rst, "From: \"AmbienteParco, WebApp\" <ambienteparco@ocdl.it>\r\n"."Bcc: pad@ocdl.it\r\n"."X-Mailer: PHP/".phpversion()."\r\n");
						} else {
							mail($_GET['Email'], "AmbienteParco, conferma prenotazione", "Buongiorno e grazie della prenotazione!\r\n\r\n".$msg."\r\n\r\nI giochi disponibili, una volta prenotati, si possono ritirare personalmente presso gli uffici di AmbienteParco dal lunedì al venerdì dalle 9.00 alle 17.00 e devono essere riportati entro il quattordicesimo giorno seguente alla prenotazione.".$rst, "From: \"AmbienteParco\" <info@ambienteparco.it>\r\n"."X-Mailer: PHP/".phpversion()."\r\n");
							// 2021-05-11, CPS. Togliere Luogo e Operatore
							mail("info@ambienteparco.it", "AmbienteParco, modificata disponibilità", "Controllate questa disponibilità:\r\n\r\n".$msg, "From: \"AmbienteParco, WebApp\" <webapp@ambienteparco.it>\r\n"."Bcc: pap@ocdl.it\r\n"."X-Mailer: PHP/".phpversion()."\r\n");
						}
						$msg="";
					}
					$dbs->exec($sql_update); // CPS, Operators will really update
				/* } else {
					$dbs->exec($sql_create); // Users only add new bookings
				} */
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF']).($msg==""?"?action=emailed":"")."'</script>";
				exit;
			}
			/* ===== RESET */
			if (isset($_GET['action']) && $_GET['action']=="reset") {
				include 'ap_sqlite.php';
				$sql_select = "SELECT rowid, * FROM $scp_Table WHERE idUtente <> '' OR Email <> ''";
				$qry2 = $dbs->query($sql_select);
				while($row2 = $qry2->fetchArray()) {
					if (hash("sha256", $row2['idEvento'].$row2['idAttivita'].(empty($row2['idUtente'])?$row2['Email']:$row2['idUtente']), false) == $_GET['idp']) {
						$sql_reset = "UPDATE $scp_Table SET idGruppo = '', idUtente = '', DataOraPresenza = '', Presenze = '', Importo = '', 
						Cognome = '', Nome = '', Email = '', Telefono = '', CAP = '', Note = ''
						WHERE rowid = '".$row2['rowid']."'";
						$dbs->exec($sql_reset);
						echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."?action=retrieve-rows&".
						"idEvento=".$row2['idEvento']."&DataOraInizio=".$_GET['date-reset']."&reset'</script>";
						exit;
					}
				}
				$dbs->close();
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DELETE */
			if (isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$dbs->exec($sql_delete);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			$dbs->close();
			setlocale(LC_TIME, 'ita', 'it_IT.utf8'); ?>

			<h2 class="alert alert-secondary"><?=ucfirst($scp_Name)?> 
				<?php /* (isset($_GET['DataOraInizio'])?" del ".strftime("%e %B %Y",strtotime($_GET['DataOraInizio'])):
					(isset($_GET['d'])?" di ".strftime("%B %Y",(int)$_GET['d']):" di ".strftime("%B %Y"))).
					(isset($row['Evento'])?" per: ".$row['Evento']:"") */ ?>
				<?=(isset($_GET['reset'])?" | <span style='color:#fff;'>Prenotazione annullata, grazie.</span>":"")?>
				<?=($_GET['action']=='emailed'?" | <span style='color:#fff;'>Inviata un'email a conferma della prenotazione, grazie.</span>":"")?>
			</h2>
			<div class="container-fluid">
				<div class="row">
					<div style="margin:0.75em 0 -1.5em 0;"><img src="img/punto-comunita-logo.png" style="float:left;max-width:75px;margin-top:-15px;">
					I giochi disponibili, una volta prenotati, si possono ritirare personalmente presso gli uffici di <i>ambienteparco</i> dal lunedì al venerdì dalle 9.00 alle 17.00 e devono essere riportati entro il quattordicesimo giorno seguente alla prenotazione.</div>
				</div>
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>"> <!--  onsubmit="this.querySelectorAll('input').forEach(i => i.disabled = false)" -->
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action']) || $_GET['action']=='retrieve-rows' || $_GET['action']=='emailed') { ?>


							<style>tbody, td, tfoot, th, thead, tr { border:none !important; }</style>
							<table border="1" style="width:100%;" class="table table-sm table-responsive table-striped align-middle table-hover" id="sortTable" data-lang="it"><!-- table-sm table-responsive align-middle -->
								<thead><?php // ##### CPS, Fields ?>
									<!-- td class="col-1">Data e Orario</td -->
									<td class="<?=(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?'col-3':'col-4')?>">Gioco, note</td>
									<?php if ($_SESSION['Ruolo']<2 && isset($_SESSION['Email'])) { ?>
										<!-- td class="col-1">Gruppo, Persona</td -->
										<!-- td class="col-1">Presenza</td -->
										<!-- td class="col-0">Luogo</td -->
										<td class="col-1">Note</td>
										<!-- td class="col-0">Pres.</td -->
										<td class="col-1">Imp.€</td>
										<td class="col-1">Operatore</td>
										<td class="col-1">Cognome, Nome</td>
										<td class="col-1">Email, Telefono</td>
									<?php } ?>
								</thead>
								<tbody>
									<?php
									// setlocale(LC_TIME, 'ita', 'it_IT');
									setlocale(LC_TIME, 'ita', 'it_IT.utf8');
									include 'ap_sqlite.php';
									$qry = $dbs->query($sql_retrieve2);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										// $msg = hash("sha256", $row['idEvento'].$row['idAttivita'].(empty($row['idUtente'])?$row['Email']:$row['idUtente']), false);
										echo "<tr class='table-row text-dark text-link'>
											<!-- td class='col-1' style='text-align:center;'><span style='display:none;'>".$row['DataOraInizio']."</span>".
											strftime("<div style='width:100px;vertical-align:middle;text-align:center;border:4px solid ".(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']) && $row['Prenotate']>0)?"#f00":"#739f2f").";border-radius:10%;background-color:#fff;line-height:1.2;font-weight:normal;'>
											<div style='font-size:2em;margin:0;background-color:".(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']) && $row['Cognome'].$row['Nome'].$row['Email'].$row['Telefono']!="")?"#f003":"#739f2f66").";border-radius:10% 10% 0 0;font-weight:bold;'>%d</div>
											<span style='font-size:1.2em;font-weight:bold;'>%A</span>".
											($_GET['action']=='retrieve-rows'?"":"<br><span style='font-size:0.8em;'>%B %Y</span>").
											($_GET['action']=='retrieve-rows'?"<br>dalle <span style='font-size:1.1em;font-weight:bold;'>%H:%M</span>":""),
											strtotime($row['DataOraInizio'])).
											($_GET['action']=='retrieve-rows'?
											strftime(" alle <span style='font-size:1.1em;font-weight:bold;'>%H:%M</span>",strtotime($row['DataOraFine'])) :"")."</div></td -->
											<td class='col-4' style='font-size:1.5em;font-weight:bold;'".(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?"":" valign='middle'")."><a href='?action=".
												(($_GET['action']!='retrieve-rows')?"update":"retrieve-rows&idEvento=".$row['idEvento'])."&RowID=".$row['rowid']."&DataOraInizio=".substr($row['DataOraInizio'],0,10)."'".
												(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']) && $row['Cognome'].$row['Nome'].$row['Email'].$row['Telefono']!="")?" style='color:#f00 !important;'>":">").$row['Evento']."</a></td>
											".(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?"
												<!-- td class='col-0'>".$row['Luogo']."</td -->
												<td class='col-1'>".$row['Note']."</td>
												<!-- td class='col-0'>".$row['Presenze']."</td -->
												<td class='col-1'>".$row['Importo']."</td>
												<td class='col-1'>".(($row['Operatore']==''||$row['Operatore']=='0')?"<span style='color:#f00;font-weight:bold;'>Operatore</span>":$row['Operatore'])."</td>
												<td class='col-1'>".$row['Cognome']." ".$row['Nome']."</td>
												<td class='col-1'>".$row['Email']." #".$row['Telefono']."</td>
											":"")."
										</tr>"; // "<td class='col-1'>".$row['Gruppo'].(empty($row['Persona'])?"":", ".$row['Persona'])."</td><td class='col-1'>".$row['DataOraPresenza']."</td>"
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>
								$('#sortTable').dataTable( {
								stateSave: true,
								"order": [[ 0, "asc" ]],
								"lengthMenu": [ 25, 50, 100, 250 ],
								"language": {
									"decimal": ",",
									"emptyTable": "Nessun dato disponibile.",
									"info": "Righe da _START_ a _END_ di _TOTAL_ totali.",
									"infoEmpty": "Elenco da 0 a 0 di 0 in totale.",
									"infoFiltered": "(filtro su _MAX_ righe)",
									"infoPostFix": "",
									"thousands": ".",
									"lengthMenu": "Elenca _MENU_ righe",
									"loadingRecords": "Lettura...",
									"processing": "Ricerca...",
									"search": "Cerca:",
									"zeroRecords": "Nessuna informazione disponibile.",
									"paginate": {
										"first": "Primo",
										"last": "ULtimo",
										"next": "Succ.",
										"previous": "Prec."
									},
									"aria": {
										"sortAscending": ": Ordine crescente",
										"sortDescending": ": Ordine decrescente"
									}
								}
							} );</script>
							<?php // ----- Create
							if (($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))) { // CPS, disabled for guests...
								?>
								<!-- input type="hidden" name="action" value="create"><button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i> Aggiungi</button -->
								<?php
							}
						/* ===== CREATE-UPDATE-DELETE ===== */
						} else { ?>

							<script src="./jquery/jquery.min.js"></script>
							<link rel="stylesheet" type="text/css" href="./jquery/jquery.datetimepicker.min.css"/>
							<script src="./jquery/jquery.datetimepicker.js"></script>

							<?php // ##### CPS, idAttivita[idEvento] x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="idAttivita">Gioco, note</label>
								<div class="col-sm-10">
									<input type="hidden" id="idEvento" name="idEvento" <?=($_GET['action']!="create"?"value='".$row['idEvento']."'":'')?>>
									<?php if ($_GET['action']!='retrievexxx') { ?>
										<input type="hidden" id="idAttivita" name="idAttivita" value="<?=$row['idAttivita']?>" >
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT e.rowid AS idEvento2, e.Descrizione || CASE WHEN e.Note = '' THEN '' ELSE ', ' || e.Note END AS Evento, 
													a.rowid AS idAttivita2, a.idEvento AS idEvento3, a.DataOraInizio, a.DataOraFine, a.idLuogo
													FROM ap_eventi AS e 
													LEFT OUTER JOIN ap_attivita AS a ON idEvento2 = idEvento3
													WHERE idEvento2 = '".$row['idEvento']."'"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											$row2 = $qry2->fetchArray()
										?>
										<input class="form-control" type="text" id="Attivita" name="Attivita" value="<?=$row2['Evento']?>" readonly> <!-- .' ('.$row2['DataOraInizio'].' / '.$row2['DataOraFine'].', '.$row2['Luogo'].')' -->
									<?php } else { ?>
										<select class="form-control" id="idAttivita" name="idAttivita" <?php echo (($_GET['action']!='retrieve' && isset($_SESSION['Email']))?'':' readonly'); // CPS, disabled for guests... ?> required>
											<option value='0'>Scegli...</option>
											<?php
												include 'ap_sqlite-row.php';
												$sql2 = "SELECT e.rowid AS idEvento2, e.Descrizione || CASE WHEN e.Note = '' THEN '' ELSE ', ' || e.Note END AS Evento, 
														a.rowid AS idAttivita2, a.idEvento AS idEvento3, a.DataOraInizio, a.DataOraFine, a.idLuogo
														FROM ap_eventi AS e 
														LEFT OUTER JOIN ap_attivita AS a ON idEvento2 = idEvento3"; // ##### CPS, Retrieve
												$qry2 = $dbs2->query($sql2);
												while($row2 = $qry2->fetchArray()) {
													echo "<option value='".$row2['idAttivita2']."'".
													(($_GET['action']!='create' && $row['idAttivita']==$row2['idAttivita2'])?" selected":"").">".
													$row2['Evento']." (".$row2['DataOraInizio']." / ".$row2['DataOraFine'].", ".$row2['Luogo'].")</option>";
												}
												$dbs2->close(); ?>
										</select>
									<?php } ?>
								</div>
							</div>

							<?php // ##### CPS, DataOraInizio x5 ?>
							<div class="row g-2 align-items-center" style="<?php echo (($_SESSION['Ruolo']<1 && isset($_SESSION['Email']))?'display:none':'display:none;'); // CPS, disabled for non-operator... ?>">
								<label class="col-sm-2 col-form-label" for="DataOraInizio">Inizio</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="DataOraInizio" name="DataOraInizio"
										<?=($_GET['action']!="create"?"value='".$row['DataOraInizio']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									$('#DataOraInizio').datetimepicker({
										lang:'it',
										i18n:{it:{months:['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugo','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre',],
											dayOfWeek:["Dom", "Lun", "Mar", "Mer","Gio", "Ven", "Sab",]}},
										format:'Y-m-d H:i',
									});
								});
							</script>
							<?php // ##### CPS, DataOraFine x5 ?>
							<div class="row g-2 align-items-center" style="<?php echo (($_SESSION['Ruolo']<1 && isset($_SESSION['Email']))?'display:none':'display:none;'); // CPS, disabled for non-operator... ?>">
								<label class="col-sm-2 col-form-label" for="DataOraFine">Fine</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="DataOraFine" name="DataOraFine"
										<?=($_GET['action']!="create"?"value='".$row['DataOraFine']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									$('#DataOraFine').datetimepicker({
										lang:'it',
										i18n:{it:{months:['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugo','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre',],
											dayOfWeek:["Dom", "Lun", "Mar", "Mer","Gio", "Ven", "Sab",]}},
										format:'Y-m-d H:i',
									});
								});
							</script>

							<?php /* ##### CPS, idGruppo+idPersona x5
							<div class="row g-2 align-items-center" style="<?php echo (isset($_SESSION['Email'])?'':'display:none;'); // CPS, disabled for guests... ?>">
								<label class="col-sm-2 col-form-label" for="idGruppo">Gruppo, Persona</label>
								<div class="col-sm-10">
									<select class="form-control" id="idGruppo" name="idGruppo" <?=($_GET['action']!='retrieve'?'':'disabled')?>>
										<option value='0'>Scegli...</option>
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT p.rowid, p.idGruppo, g.Descrizione || ', ' || g.Note AS Gruppo FROM ap_prenotazioni AS p LEFT JOIN ap_gruppi AS g ON p.idGruppo = g.rowid WHERE p.rowid = '".$_GET['RowID']."'"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['rowid']."'".
												(($_GET['action']!='create' && $row['idGruppo']==$row2['rowid'])?" selected":"").">".
												$row2['Gruppo']."</option>";
											}
											$dbs2->close();
										?>
									</select>
								</div>
							</div> */ ?>

							<?php // ##### CPS, DataOraPresenza x5 ?>
							<div class="row g-2 align-items-center" style="<?php echo (isset($_SESSION['Email'])?'':'display:none;'); // CPS, disabled for guests... ?>">
								<label class="col-2 col-form-label" for="DataOraPresenza">Consegna/Ritiro</label><div class="col-sm-10">
								<input class="form-control" type="text" id="DataOraPresenza" name="DataOraPresenza" 
									<?=($_GET['action']!="create"?"value='".$row['DataOraPresenza']."'":"").($_GET['action']=="retrieve"?' disabled':'')?>></div>
							</div>
							<script type="text/javascript">
								$(document).ready(function() {
									$('#DataOraPresenza').datetimepicker({
										lang:'it',
										i18n:{it:{months:['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugo','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre',],
											dayOfWeek:["Dom", "Lun", "Mar", "Mer","Gio", "Ven", "Sab",]}},
										format:'Y-m-d H:i',
									});
								});
							</script>

							<?php // ##### CPS, Presenze x5 ?>
							<!-- div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Presenze"><?=(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?'Presenti':'Persone partecipanti?')?></label>
								<div class="col-sm-10" -->
									<input class="form-control" type="hidden" id="Presenze" name="Presenze" min="0" max="<?=$row ['Posti']?>" step="1"
										<?=($_GET['action']!="create"?"value='".$row['Presenze']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								<!-- /div>
							</div -->
							<?php // ##### CPS, Importo x5 ?>
							<div class="row g-2 align-items-center" style="<?php echo (isset($_SESSION['Email'])?'':'display:none;'); // CPS, disabled for guests... ?>">
								<label class="col-sm-2 col-form-label" for="Importo">Importo</label>
								<div class="col-sm-10">
									<input class="form-control" type="number" id="Importo" name="Importo" min="0.00" max="999999.00" step="0.50" <?php echo (isset($_SESSION['Email'])?"":" disabled"); // CPS, disabled for guests... ?>
										<?=($_GET['action']!="create"?"value='".$row['Importo']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>

							<?php // ##### CPS, idOperatore x4
							if (isset($_SESSION['Email'])) { ?>
							<div class="row g-2 align-items-center" style="<?php echo (isset($_SESSION['Email'])?'':'display:none;'); // CPS, disabled for guests... ?>">
								<label class="col-sm-2 col-form-label" for="idOperatore" <?=(($row['idOperatore']==''||$row['idOperatore']=='0')?" style='color:#f00;font-weight:bold;'":"")?>>Operatore</label>
								<div class="col-sm-10">
									<select class="form-control" id="idOperatore" name="idOperatore" <?php echo (($_GET['action']!='retrieve' && isset($_SESSION['Email']))?'':'disabled'); // CPS, disabled for guests... ?> required>
										<option value='0'>Scegli...</option>
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT rowid, * FROM ap_operatori"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['rowid']."'".
												(($_GET['action']!='create' && $row['idOperatore']==$row2['rowid'])?" selected":"").">".
												$row2['Cognome'].' '.$row2['Nome']."</option>";
											}
											$dbs2->close();
										?>
									</select>
								</div>
							</div>
							<?php } else { ?>
								<input type="hidden" id="idOperatore" name="idOperatore" value="<?=$row['idOperatore']?>">
							<?php } ?>

							<?php // ##### CPS, Cognome Nome x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Cognome"><?=(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?'Cognome, Nome referente':'Cognome, Nome referente?')?></label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Cognome" name="Cognome" placeholder="Cognome"
										<?=($_GET['action']!="create"?"value='".$row['Cognome']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
									<input class="form-control" type="text" id="Nome" name="Nome" placeholder="Nome"
										<?=($_GET['action']!="create"?"value='".$row['Nome']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, Email Telefono x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Email"><?=(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?'Email, Telefono referente':'Email, Telefono referente?')?></label>
								<div class="col-sm-10">
									<input class="form-control" type="email" id="Email" name="Email" placeholder="Email, valida"
										<?=($_GET['action']!="create"?"value='".$row['Email']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
									<input class="form-control" type="text" id="Telefono" name="Telefono" placeholder="Telefono, cellulare"
										<?=($_GET['action']!="create"?"value='".$row['Telefono']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, CAP-Comune x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="CAP"><?=(($_SESSION['Ruolo']<2 && isset($_SESSION['Email']))?'CAP, Comune referente':'CAP, Comune referente?')?></label>
								<div class="col-sm-10">
									<script src="js/bootstrap-combobox.js"></script>
									<link href="css/bootstrap-combobox.css" rel="stylesheet">
									<select class="form-control combobox" id="CAP" name="CAP">
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT rowid, * FROM ap_capcomuni"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['CAP']."'".
												(($_GET['action']!='create' && $row['CAP']==$row2['CAP'])?" selected":"").">".
												$row2['CAP'].', '.$row2['Comune']."</option>";
											}
											$dbs2->close();
										?>
										<option value='' <?=(($_GET['action']=='create' || $row['CAP']=='')?'selected':'')?> placeholder="CAP, Comune...">Indicare CAP oppure Comune...</option>
									</select>
									<script type="text/javascript">
										$(document).ready(function(){
											$('.combobox').combobox(); // {renderLimit: 12}
										});
									</script>
								</div>
							</div>
							<?php if ($_SESSION['Ruolo']<2 && isset($_SESSION['Email'])) { ?>
								<?php // ##### CPS, Note x5 ?>
								<div class="row g-2 align-items-center">
									<label class="col-sm-2 col-form-label" for="Note">Note, riservate</label>
									<div class="col-sm-10">
										<input class="form-control" type="text" id="Note" name="Note" placeholder="Note riservate"
											<?=($_GET['action']!="create"?"value='".$row['Note']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
									</div>
								</div>
							<?php } ?>
							<?php // ##### CPS, Privacy
							if (!isset($_SESSION['Email'])) { ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label">Hai letto l'<a href="https://www.ambienteparco.it/pdf/privacy_policy.pdf">informativa?</a></label>
								<div class="col-sm-10" style="margin-left:20px; margin-right:-20px;">
									<input class="form-check-input" type="checkbox" required>&nbsp;Consento al trattamento dei dati nel rispetto della privacy</input>
								</div>
							</div>
							<?php } ?>

							<?php // ################################################################################ ?>
							<p>&nbsp;</p>
							<input type="hidden" name="RowID" value="<?=$row['rowid']?>">
							<?php   // ----- SUBMIT, create-save
							if ($_GET['action']=='create' && $_SESSION['Ruolo']<2 && isset($_SESSION['Email'])) { // CPS, disabled for guests... ?>
								<button class="btn btn-success" type="submit" name="action" value="create-save">
									<i class="fa fa-check-square"></i>&nbsp;Salva</button>
							<?php } // ----- SUBMIT, duplicate-save
							if ($_GET['action']=='update' && $_GET['action']!='create' && $_SESSION['Ruolo']<2 && isset($_SESSION['Email'])) { ?>
								<button class="btn btn-success" type="submit" name="action" value="duplicate-save">
									<i class="fa fa-plus-square"></i>&nbsp;Salva e Libera</button>
							<?php } // ----- SUBMIT, update-save
							if ($_GET['action']=='update') { ?>
								<button class="btn btn-primary" type="submit" name="action" value="update-save">
									<i class="fa fa-check-square"></i>&nbsp;<?=(isset($_SESSION['Email'])?"Salva":"<b>PRENOTA</b>")?>
								</button>
							<?php } // ----- SUBMIT, update [guest-booking]
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=update&RowID=<?=$row['rowid']?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;<?=(isset($_SESSION['Email'])?"Modifica":"<b>PRENOTA...</b>")?></a>
							<?php } // ----- SUBMIT, Alert delete
							if ($_SESSION['Ruolo']<2 && isset($_SESSION['Email'])) { // CPS, disabled for guests... ?>
								<a href="javascript: confirm_delete(<?=$row['rowid']?>);" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Elimina</a>
								<script type="text/javascript">
								function confirm_delete( rid, uid ) {
									if (confirm('Eliminare la riga '+ rid + ' ?')) {
										window.location.href = '<?=htmlentities($_SERVER['PHP_SELF'])?>?action=delete&RowID=' + rid;
									}
								}
								</script>
							<?php } // ----- Back ?>
							<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i>&nbsp;Ritorna</a>
						<?php } ?>
					</div>
				</form>
			</div>
	<?php include 'ap_footer.php'; ?>
