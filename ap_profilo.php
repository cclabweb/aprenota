<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage in "database.sqlite" the data in table "ap_profilo":
 * Field					Type		!N	Value	Key
 * ------------------------+-----------+---+-------+----
 * Cognome					TEXT		No	None	No
 * Nome						TEXT		No	None	No
 * Username					TEXT		Yes	None	No
 * Password					TEXT		Yes	None	No
 * idRuolo					INTEGER		No	None	No
 * Email					TEXT		Yes	None	No
 * Telefono					TEXT		No	None	No
 * Immagine					TEXT		No	None	No
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - ?action = Create, Retrieve (RowID/All), Update (RowID) or Delete (RowID)
 * then comes two form:
 * - Retrieve all, when no action (DataTable: responsive, search, sort, pagination)
 * - Retrieve RowID, for Create, Update, Delete actions with details and validation
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}

		// ################################################################################
		$scp_name ="Profilo";
		$scp_table="ap_"."operatori";
		$sql_create = "";
		$sql_retrieve1 = "SELECT rowid, * FROM $scp_table WHERE Email = '".$_SESSION['Email']."'";
		$sql_update = "UPDATE $scp_table SET 
					Cognome = '".$_GET['Cognome']."', Nome = '".$_GET['Nome']."', Username = '".$_GET['Username']."', Password = '".md5($_GET['Password'])."', 
					idRuolo = '".$_GET['idRuolo']."', Email = '".$_GET['Email']."', Telefono = '".$_GET['Telefono']."' WHERE Email = '".$_SESSION['Email']."'";
		$sql_delete = "DELETE FROM $scp_table WHERE Email = '".$_SESSION['Email']."'";
		$sql_retrieve2 = "SELECT rowid, * FROM $scp_table WHERE Email = '".$_SESSION['Email']."'";
		// ################################################################################

		include 'ap_header.php'; ?>
		<title><?=ucfirst($scp_name)?></title>
		<script>
			var a = document.getElementById("menu-<?=$scp_name?>");
			a.classList.add("active");  
		</script>

	<?php include 'ap_menu.php'; ?>

			<h2 class="alert alert-secondary"><?=ucfirst($scp_name)?> | <?=$_SESSION['Cognome']." ".$_SESSION['Nome']?></h2>
			<?php
			include 'ap_sqlite.php';
			/* ===== CREATE */
			if (isset($_GET['action']) && $_GET['action']=="create-save") {
				$dbs->exec($sql_create);
				// ################################################################################
				$frm = "From: \"PAP\" <pap@ocdls.it>"."\r\n"."Reply-To: <pap@ocdl.it>"."\r\n"."X-Mailer: PHP/".phpversion();
				$sql = "SELECT rowid, * FROM $scp_table WHERE Email = '".htmlspecialchars($_GET['Email'])."'";
				$qry = $dbs->query($sql);
				$row = $qry->fetchArray();
				if ($row > 0 && $row['Email'] === htmlspecialchars($_GET['Email'])) {
					echo "1";
					echo ((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']))?"https":"https")."://".
					$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).
					"?reset=".$row['Password'];
					exit;
					mail(	$row['Email'], "Richiesta di IMPOSTAZIONE PASSWORD per ".
							htmlspecialchars($_GET['Email']), "ATTENZIONE... Ricevuta una richiesta di IMPOSTAZIONE della Password per ".
							$row['Email'].". Se volete annullare questa richiesta non fate nulla, se invece volete confermala indicate una nuova Password in questa pagina: ".
							((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']))?"https":"https")."://".$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."?reset=".$row['Password'], $frm);
				}
				// ################################################################################
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== RETRIEVE-ONE */
			if (isset($_GET['action']) && ($_GET['action']=="retrieve" || $_GET['action']=="update") && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$qry = $dbs->query($sql_retrieve1);
				$row = $qry->fetchArray();
			}
			/* ===== UPDATE */
			if (isset($_GET['action']) && $_GET['action']=="update-save") {
				$dbs->exec($sql_update);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DELETE */
			if (isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$dbs->exec($sql_delete);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			$dbs->close(); ?>
			<div class="container-fluid">
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>">
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action'])) { ?>
							<?php // ################################################################################ ?>

							<table border="1" style="width:100%;" class="table table-sm table-responsive table-striped align-middle table-hover" id="sortTable" data-lang="it">
								<thead><?php // ##### CPS, Fields ?>
									<td class="col-8 d-xs-table-cell d-sm-none">Nominativo</td>
									<td class="col-2 d-none d-sm-table-cell">Cognome</td>
									<td class="col-2 d-none d-sm-table-cell">Nome</td>
									<td class="col-1 d-none d-md-table-cell">Username</td>
									<td class="col-1 d-none d-md-table-cell">Password</td>
									<td class="col-1 ruolo">Ruolo</td>
									<td class="col-1 d-none d-md-table-cell">Email</td>
									<td class="col-1 d-none d-md-table-cell">Telefono</td>
								</thead>
								<tbody>
									<?php
									include 'ap_sqlite.php';
									$qry = $dbs->query($sql_retrieve2);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										echo "<tr class='table-row text-dark text-link'>
											<td class='col-8 d-xs-table-cell d-sm-none'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Cognome']." ".$row['Nome']."</a></td>
											<td class='col-2 d-none d-sm-table-cell'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Cognome']."</a></td>
											<td class='col-2 d-none d-sm-table-cell'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Nome']."</a></td>
											<td class='col-2 d-none d-md-table-cell'>".$row['Username']."</td>
											<td class='col-1 d-none d-md-table-cell'>".substr("*************************",1,strlen($row['Password']))."</td>";
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT rowid, * FROM ap_ruoli WHERE Ruolo = '".$row['idRuolo']."'";
											$qry2 = $dbs2->query($sql2);
											$row2 = $qry2->fetchArray();
											if ($row2 > 0) {
												echo "<td class='col-1 ruolo'>".$row2['Descrizione']."</td>";
											}
											$dbs2->close();
										echo "
											<td class='col-2 d-none d-md-table-cell'>".$row['Email']."</td>
											<td class='col-2 d-none d-md-table-cell'>".$row['Telefono']."</td>
										</tr>";
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>$('#sortTable').dataTable( { "lengthMenu": [ 5, 10, 50, 100 ], "language": { "decimal": ",", "emptyTable": "Nessun dato disponibile.", "info": "Righe da _START_ a _END_ di _TOTAL_ totali.", "infoEmpty": "Elenco da 0 a 0 di 0 in totale.", "infoFiltered": "(filtro su _MAX_ righe)", "infoPostFix": "", "thousands": ".", "lengthMenu": "Elenca _MENU_ righe", "loadingRecords": "Lettura...", "processing": "Ricerca...", "search": "Cerca:", "zeroRecords": "Nessuna informazione disponibile.", "paginate": { "first": "Primo", "last": "ULtimo", "next": "Succ.", "previous": "Prec." }, "aria": { "sortAscending": ": Ordine crescente", "sortDescending": ": Ordine decrescente" } } } );</script>
							<?php // ----- Create ?>
							<!-- input type="hidden" name="action" value="create"><button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i> Aggiungi</button -->
							<?php
						/* ===== CREATE-UPDATE-DELETE ===== */
						} else { ?>
							<?php // ##### CPS, Cognome x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Cognome">Cognome</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Cognome" name="Cognome" 
										<?=($_GET['action']!="create"?"value='".$row['Cognome']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, Nome x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Nome">Nome</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Nome" name="Nome" 
										<?=($_GET['action']!="create"?"value='".$row['Nome']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, Username x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Username">Nome utente</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Username" name="Username" 
										<?=($_GET['action']!="create"?"value='".$row['Username']."'":'').($_GET['action']=="retrieve"?' disabled':'')?> required>
								</div>
							</div>
							<?php // ##### CPS, Password[Validation...] x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Password">Password</label>
								<div class="col-sm-10">
									<input class="form-control" type="<?=($_GET['action']!='create'?'password':'text')?>" id="Password" name="Password" 
									    value="<?=($_GET['action']!="create"?$row['Password'].'':'L\'utente riceverà una email per impostare la Password')?>" data-toggle="password" disabled required>
									<button type="button" id="togglePasssword" style="height: 0px; width: 0px; padding: 0px; margin: 0px; position: absolute; top: 8px; right: 42px; color: #888;">
										<i class="fa fa-eye-slash"></i></button>
									<script>
										pwd = document.querySelector('#Password');
										button = document.querySelector('#togglePasssword');
										button.addEventListener('click', (e) => {
											if(pwd.type == 'password'){
												e.target.setAttribute('class', 'fa fa-eye');
												pwd.type = 'text';
											} else {
												e.target.setAttribute('class', 'fa fa-eye-slash');
												pwd.type = 'password';
											}
										});
									</script>
								</div>
							</div>
							<?php // ##### CPS, Ruoli[Ruolo(Descrizione)->idRuolo(Nome)] x4+3 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="idRuolo">Ruolo</label>
								<div class="col-sm-10">
									<select class="form-control" id="idRuolo" name="idRuolo" <?=($_GET['action']!='retrieve'?'':'disabled')?>>
										<option value='0'>Scegli...</option>
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT rowid, * FROM ap_ruoli"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['Ruolo']."'".
												(($_GET['action']!='create' && $row['idRuolo']==$row2['Ruolo'])?" selected":"").">".
												$row2['Ruolo'].") ".$row2['Descrizione']."</option>";
											}
											$dbs2->close();
										?>
									</select>
								</div>
							</div>
							<?php // ##### CPS, Email[Validation...] x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Email">Email</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Email" name="Email" 
										pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" placeholder="Indicare un indirizzo Email valido"
										<?=($_GET['action']!="create"?"value='".$row['Email']."'":'').($_GET['action']=="retrieve"?' disabled':'')?> required>
								</div>
							</div>
							<?php // ##### CPS, Telefono x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Telefono">Telefono</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Telefono" name="Telefono" 
										<?=($_GET['action']!="create"?"value='".$row['Telefono']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>

							<?php // ################################################################################ ?>
							<p>&nbsp;</p>
							<?php   // ----- SUBMIT, create-save
							if ($_GET['action']=='create' && 1==2 ) { // CPS, DISABLED!!! ?>
								<input type="hidden" name="action" value="create-save"><button class="btn btn-success" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update-save
							if ($_GET['action']=='update') { ?>
								<input type="hidden" name="RowID" value="<?=$row['rowid']?>"><input type="hidden" name="action" value="update-save"><button class="btn btn-primary" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=update&RowID=<?=$row['rowid']?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Modifica</a>
							<?php   // ----- SUBMIT, Alert delete | CPS, DISABLED!!! ?>
								<!-- a href="javascript: confirm_delete(<?php // $row['rowid']; ?>);" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Elimina</a>
								<script type="text/javascript">
								function confirm_delete( rid, uid ) {
									if (confirm('Eliminare la riga '+ rid + ' ?')) {
										window.location.href = '<?php // htmlentities($_SERVER['PHP_SELF']); ?>?action=delete&RowID=' + rid;
									}
								}
								</script -->
							<?php } // ----- Back ?>
							<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i>&nbsp;Ritorna</a>
						<?php } ?>
					</div>
				</form>
			</div>

	<?php include 'ap_footer.php'; ?>
