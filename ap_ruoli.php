<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage in "database.sqlite" the data in table "ap_ruoli":
 * Field					Type		!N	Value	Key
 * ------------------------+-----------+---+-------+----
 * Ruolo					TEXT		No	None	No
 * Descrizione				TEXT		No	None	No
 * fgOperatori				TEXT		No	None	No
 * fgUtenti					TEXT		No	None	No
 * fgAttivita				TEXT		No	None	No
 * fgLuogo					TEXT		No	None	No
 * fgRuoli					TEXT		No	None	No
 * fgTipi					TEXT		No	None	No
 * fgIncarichi				TEXT		No	None	No
 * fgVincoli				TEXT		No	None	No
 * fgPrenotazioni			TEXT		No	None	No
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - Action = Create, Retrieve (One-RowID), Update (RowID) or Delete (RowID)
 * then comes two form:
 * - Retrieve All, when no action (DataTable: responsive, search, sort, pagination)
 * - Retrieve One-RowID for Create, Update, Delete actions with details, validation
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}

		// ################################################################################ CPS. Title-Table-Name and 5 SQL Queries (CR12UD)
		$scp_name ="ruoli";
		$scp_table="ap_".$scp_name;
		$sql_create = "INSERT INTO $scp_table (Ruolo, Descrizione) VALUES ('".$_GET['Ruolo']."', '".$_GET['Descrizione']."')";
		$sql_retrieve1 = "SELECT rowid, * FROM $scp_table WHERE rowid = '".$_GET['RowID']."'";
		$sql_update = "UPDATE $scp_table SET Ruolo = '".$_GET['Ruolo']."', Descrizione = '".$_GET['Descrizione']."' WHERE rowid = '".$_GET['RowID']."'";
		$sql_delete = "DELETE FROM $scp_table WHERE rowid = '".$_GET['RowID']."'";
		$sql_retrieve2 = "SELECT rowid, * FROM $scp_table ORDER BY Ruolo";
		// ################################################################################

		include 'ap_header.php'; ?>
		<title><?=ucfirst($scp_name)?></title>
		<script>
			var a = document.getElementById("menu-<?=$scp_name?>");
			a.classList.add("active");  
		</script>

	<?php include 'ap_menu.php'; ?>

			<h2 class="alert alert-secondary"><?=ucfirst($scp_name)?></h2>
			<?php
			include 'ap_sqlite.php';
			/* ===== CREATE */
			if (isset($_GET['action']) && $_GET['action']=="create-save") {
				$dbs->exec($sql_create);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== RETRIEVE-ONE */
			if (isset($_GET['action']) && ($_GET['action']=="retrieve" || $_GET['action']=="update") && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$qry = $dbs->query($sql_retrieve1);
				$row = $qry->fetchArray();
			}
			/* ===== UPDATE */
			if (isset($_GET['action']) && $_GET['action']=="update-save") {
				$dbs->exec($sql_update);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DELETE */
			if (isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$dbs->exec($sql_delete);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			$dbs->close(); ?>
			<div class="container-fluid">
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>">
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action'])) { ?>
							<?php // ################################################################################ ?>

							<table border="1" class="table table-sm table-responsive table-striped align-middle table-hover" class="table" id="sortTable" data-lang="it">
								<thead><?php // ##### CPS, Fields ?>
									<td class="col-6">Ruolo</td>
									<td class="col-6">Descrizione</td>
								</thead>
								<tbody>
									<?php
									include 'ap_sqlite.php';
									$qry = $dbs->query($sql_retrieve2);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										echo "<tr class='table-row text-dark text-link'>
											<td class='col-6'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Ruolo']."</a></td>
											<td class='col-6'>".$row['Descrizione']."</td>
										</tr>";
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>$('#sortTable').dataTable( { "lengthMenu": [ 5, 10, 50, 100 ], "language": { "decimal": ",", "emptyTable": "Nessun dato disponibile.", "info": "Righe da _START_ a _END_ di _TOTAL_ totali.", "infoEmpty": "Elenco da 0 a 0 di 0 in totale.", "infoFiltered": "(filtro su _MAX_ righe)", "infoPostFix": "", "thousands": ".", "lengthMenu": "Elenca _MENU_ righe", "loadingRecords": "Lettura...", "processing": "Ricerca...", "search": "Cerca:", "zeroRecords": "Nessuna informazione disponibile.", "paginate": { "first": "Primo", "last": "ULtimo", "next": "Succ.", "previous": "Prec." }, "aria": { "sortAscending": ": Ordine crescente", "sortDescending": ": Ordine decrescente" } } } );</script>
							<?php // ----- Create ?>
							<input type="hidden" name="action" value="create"><button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i> Aggiungi</button>
							<?php
						/* ===== CREATE-UPDATE-DELETE ===== */
						} else { ?>
							<?php // ##### CPS, Ruolo x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-6 col-form-label" for="Ruolo">Ruolo</label><div class="col-sm-10">
								<input type="text" class="form-control" id="Ruolo" name="Ruolo" 
									<?=($_GET['action']!="create"?"value='".$row['Ruolo']."'":'').($_GET['action']=="retrieve"?' disabled':'')?> required></div>
							</div>
							<?php // ##### CPS, Descrizione x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-6 col-form-label" for="Posti">Descrizione</label><div class="col-sm-10">
								<input class="form-control" type="text" id="Descrizione" name="Descrizione" 
									<?=($_GET['action']!="create"?"value='".$row['Descrizione']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>></div>
							</div>

							<?php // ################################################################################ ?>
							<p>&nbsp;</p>
							<?php   // ----- SUBMIT, create-save
							if ($_GET['action']=='create') { ?>
								<input type="hidden" name="action" value="create-save"><button class="btn btn-success" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update-save
							if ($_GET['action']=='update') { ?>
								<input type="hidden" name="RowID" value="<?=$row['rowid']?>"><input type="hidden" name="action" value="update-save"><button class="btn btn-primary" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=update&RowID=<?=$row['rowid']?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Modifica</a>
							<?php   // ----- SUBMIT, Alert delete ?>
								<a href="javascript: confirm_delete(<?=$row['rowid']?>);" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Elimina</a>
								<script type="text/javascript">
								function confirm_delete( rid, uid ) {
									if (confirm('Eliminare la riga '+ rid + ' ?')) {
										window.location.href = '<?=htmlentities($_SERVER['PHP_SELF'])?>?action=delete&RowID=' + rid;
									}
								}
								</script>
							<?php } // ----- Back ?>
							<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i>&nbsp;Ritorna</a>
						<?php } ?>
					</div>
				</form>
			</div>

	<?php include 'ap_footer.php'; ?>
