<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * Include this script to open SQLite database, inside another Query on the same DB
 * and, again, dont forget to "$dbs2->close();"!
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
	$dbs2 = new SQLite3('database.sqlite');
	$dbs2->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
	$dbs2->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
