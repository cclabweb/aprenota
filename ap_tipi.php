<?php
/* ================================================================================
 * Web App named "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage in "database.sqlite" the data in table "ap_tipi":
 * Field					Type	!N	Value	Key
 * ------------------------+-------+---+-------+----
 * Livello					INTEGER	No	None	No
 * Descrizione				TEXT	No	None	No
 * Note						TEXT	No	None	No
 * idUtente_Partner			INTEGER	No	None	No
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - ?action = Create, Retrieve (RowID/All), Update (RowID) or Delete (RowID)
 * then comes two form:
 * - Retrieve all, when no action (DataTable: responsive, search, sort, pagination)
 * - Retrieve RowID, for Create, Update, Delete actions with details and validation
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-12-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}
		include 'ap_header.php'; ?>
		<title>Tipi</title><?php // ##### CPS, Title ?>
		<script>  
			var a = document.getElementById("menu-Tipi");  
			a.classList.add("active");  
		</script>

	<?php include 'ap_menu.php'; ?>

			<h2 class="alert alert-secondary">Tipi</h2><?php // ##### CPS, Title ?>
			<?php
			$dbs = new SQLite3('database.sqlite');
			$dbs->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
			$dbs->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
			/* ===== CREATE ##### CPS, Query */
			if (isset($_GET['action']) && $_GET['action']=="create-save") {
				$sql = "INSERT INTO ap_tipi (Livello, Descrizione, Note, idUtente_Partner) 
						VALUES ('".$_GET['Livello']."', '".$_GET['Descrizione']."', '".$_GET['Note']."', '".$_GET['idUtente_Partner']."')";
				$dbs->exec($sql);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== RETRIEVE-ONE ##### CPS, Query */
			if (isset($_GET['action']) && ($_GET['action']=="retrieve" || $_GET['action']=="update") && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$sql = "SELECT rowid, * FROM ap_Tipi WHERE rowid = '".$_GET['RowID']."'";
				$qry = $dbs->query($sql);
				$row = $qry->fetchArray();
			}
			/* ===== UPDATE ##### CPS, Query */
			if (isset($_GET['action']) && $_GET['action']=="update-save") {
				$sql = "UPDATE ap_Tipi SET 
				Livello = '".$_GET['Livello']."', Descrizione = '".$_GET['Descrizione']."', 
				Note = '".$_GET['Note']."', idUtente_Partner = '".$_GET['idUtente_Partner']."' WHERE rowid = '".$_GET['RowID']."'";
				$dbs->exec($sql);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DELETE ##### CPS, Query */
			if (isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$sql = "DELETE FROM ap_Tipi WHERE rowid = '".$_GET['RowID']."'";
				$dbs->exec($sql);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			$dbs->close(); ?>
			<div class="container-fluid">
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>">
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action'])) { ?>
							<table border="1" style="width:100%;" class="table table-sm table-responsive table-striped align-middle table-hover" id="sortTable" data-lang="it">
								<thead><?php // ##### CPS, Fields ?>
									<td class="col-4">Livello</td>
									<td class="col-4">Descrizione</td>
									<td class="col-4 d-none d-md-table-cell">Note</td>
									<td class="col-4 d-none d-md-table-cell">Partner</td>
								</thead>
								<tbody>
									<?php
									$dbs = new SQLite3('database.sqlite');
									$dbs->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
									$dbs->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
									$sql = "SELECT rowid, * FROM ap_Tipi"; // ##### CPS, Query
									$qry = $dbs->query($sql);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										echo "<tr class='table-row text-dark text-link'>
											<td class='col-2'>".$row['Livello']."</td>
											<td class='col-2'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Descrizione']."</a></td>
											<td class='col-2 d-none d-md-table-cell'>".$row['Note']."</td>
											<td class='col-2 d-none d-md-table-cell'>".$row['idUtente_Partner']."</td>
										</tr>";
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>$('#sortTable').dataTable( { "lengthMenu": [ 50, 100, 500 ], "language": { "decimal": ",", "emptyTable": "Nessun dato disponibile.", "info": "Righe da _START_ a _END_ di _TOTAL_ totali.", "infoEmpty": "Elenco da 0 a 0 di 0 in totale.", "infoFiltered": "(filtro su _MAX_ righe)", "infoPostFix": "", "thousands": ".", "lengthMenu": "Elenca _MENU_ righe", "loadingRecords": "Lettura...", "processing": "Ricerca...", "search": "Cerca:", "zeroRecords": "Nessuna informazione disponibile.", "paginate": { "first": "Primo", "last": "ULtimo", "next": "Succ.", "previous": "Prec." }, "aria": { "sortAscending": ": Ordine crescente", "sortDescending": ": Ordine decrescente" } } } );</script>
							<?php // ----- Create ?>
							<input type="hidden" name="action" value="create"><button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i> Aggiungi</button>
							<?php
						/* ===== CREATE-UPDATE-DELETE ===== */
						} else { ?>
							<?php // ##### CPS, Livello x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Livello">Livello</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Livello" name="Livello" 
										<?=($_GET['action']!="create"?"value='".$row['Livello']."'":'').($_GET['action']=="retrieve"?' disabled':'')?> required>
								</div>
							</div>
							<?php // ##### CPS, Descrizione x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Descrizione">Descrizione</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Descrizione" name="Descrizione" 
										<?=($_GET['action']!="create"?"value='".$row['Descrizione']."'":'').($_GET['action']=="retrieve"?' disabled':'')?> required>
								</div>
							</div>
							<?php // ##### CPS, Note x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Note">Note</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Note" name="Note" 
										<?=($_GET['action']!="create"?"value='".$row['Note']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, ap_utenti[rowid(Cognome.Nome)->idUtente_Partner(Nominativo)] x4+3 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="idUtente_Partner">Nominativo</label>
								<div class="col-sm-10">
									<select class="form-control" id="idUtente_Partner" name="idUtente_Partner" <?=($_GET['action']!='retrieve'?'':'disabled')?>>
										<option value='0'>Scegli...</option>
										<?php
											$dbs2 = new SQLite3('database.sqlite');
											$dbs2->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
											$dbs2->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
											$sql2 = "SELECT rowid, * FROM ap_utenti";
											$qry2 = $dbs2->query($sql2);
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['rowid']."'".
												(($_GET['action']!='create' && $row['idUtente_Partner']==$row2['rowid'])?" selected":"").">".
												$row2['rowid'].") ".$row2['Cognome']." ".$row2['Nome']."</option>";
											}
											$dbs2->close();
										?>
									</select>
								</div>
							</div>

							<p>&nbsp;</p>
							<?php   // ----- SUBMIT, create-save
							if ($_GET['action']=='create') { ?>
								<input type="hidden" name="action" value="create-save"><button class="btn btn-success" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update-save
							if ($_GET['action']=='update') { ?>
								<input type="hidden" name="RowID" value="<?=$row['rowid']?>"><input type="hidden" name="action" value="update-save"><button class="btn btn-primary" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=update&RowID=<?=$row['rowid']?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Modifica</a>
							<?php   // ----- SUBMIT, Alert delete ?>
								<a href="javascript: confirm_delete(<?=$row['rowid']?>);" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Elimina</a>
								<script type="text/javascript">
								function confirm_delete( rid, uid ) {
									if (confirm('Eliminare la riga '+ rid + ' ?')) {
										window.location.href = '<?=htmlentities($_SERVER['PHP_SELF'])?>?action=delete&RowID=' + rid;
									}
								}
								</script>
							<?php } // ----- Back ?>
							<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i>&nbsp;Ritorna</a>
						<?php } ?>
					</div>
				</form>
			</div>

	<?php include 'ap_footer.php'; ?>
