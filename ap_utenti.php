<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * One page-script to manage in "database.sqlite" the data in table "ap_utenti":
 * Field					Type	!N	Value	Key
 * ------------------------+-------+---+-------+----
 * Cognome					TEXT	No	None	No
 * Nome						TEXT	No	None	No
 * Denominazione			TEXT	No	None	No
 * Email					TEXT	No	None	No
 * Telefono					TEXT	No	None	No
 * username					TEXT	No	None	No
 * password					TEXT	No	None	No
 * idGruppo					INTEGER	No	None	No
 * --------------------------------------------------------------------------------
 * At first there are the primary four action:
 * - ?action = Create, Retrieve (RowID/All), Update (RowID) or Delete (RowID)
 * then comes two form:
 * - Retrieve all, when no action (DataTable: responsive, search, sort, pagination)
 * - Retrieve RowID, for Create, Update, Delete actions with details and validation
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-03-29 21.57
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
		session_start();
		if (!isset($_SESSION['Email'])) {
			header("Location: ".htmlentities($_SERVER['PHP_SELF']));
			exit;
		}
		// ################################################################################
		$scp_name	="persone";
		$scp_table	="ap_"."utenti";
		$sql_create = "INSERT INTO $scp_table (Cognome, Nome, Denominazione, Email, Telefono, username, password, idGruppo) 
					VALUES ('".$_GET['Cognome']."', '".$_GET['Nome']."', '".$_GET['Denominazione']."', '".$_GET['Email']."', '".$_GET['Telefono']
					."', '".$_GET['username']."', '".$_GET['password']."', '".$_GET['idGruppo']."')";
		$sql_retrieve1 = "SELECT u.rowid, u.Cognome, u.Nome, u.Denominazione, u.Email, u.Telefono, u.username, u.password, u.idGruppo, g.Descrizione AS Gruppo
					FROM $scp_table AS u LEFT JOIN ap_gruppi AS g ON u.idGruppo = g.rowid WHERE u.rowid = '".$_GET['RowID']."'";
		$sql_update	= "UPDATE $scp_table SET
					Cognome = '".$_GET['Cognome']."', Nome = '".$_GET['Nome']."', Denominazione = '".$_GET['Denominazione']."', Email = '".$_GET['Email']."', Telefono = '".$_GET['Telefono']."', 
					username = '".$_GET['username']."', username = '".$_GET['username']."', password = '".$_GET['password']."', idGruppo = '".$_GET['idGruppo']."' 
					WHERE rowid = '".$_GET['RowID']."'";
		$sql_delete	= "DELETE FROM $scp_table WHERE rowid = '".$_GET['RowID']."'";
		$sql_retrieve2 = "SELECT u.rowid, u.Cognome, u.Nome, u.Denominazione, u.Email, u.Telefono, u.username, u.password, u.idGruppo, g.Descrizione AS Gruppo
					FROM $scp_table AS u LEFT JOIN ap_gruppi AS g ON u.idGruppo = g.rowid";
		// ################################################################################

		include 'ap_header.php'; ?>
		<title><?=ucfirst($scp_name)?></title>
		<script>
			var a = document.getElementById("menu-<?=$ap_Name?>");
			a.classList.add("active");  
		</script>

	<?php include 'ap_menu.php'; ?>

			<h2 class="alert alert-secondary"><?=ucfirst($scp_name)?></h2>
			<?php
			include 'ap_sqlite.php';
			/* ===== CREATE */
			if (isset($_GET['action']) && $_GET['action']=="create-save") {
				$dbs->exec($sql_create);
				// ################################################################################
				$frm = "From: \"PAP\" <pap@ocdls.it>"."\r\n"."Reply-To: <pap@ocdl.it>"."\r\n"."X-Mailer: PHP/".phpversion();
				$sql = "SELECT rowid, * FROM $scp_table WHERE Email = '".htmlspecialchars($_GET['Email'])."'";
				$qry = $dbs->query($sql);
				$row = $qry->fetchArray();
				if ($row > 0 && $row['Email'] === htmlspecialchars($_GET['Email'])) {
					// echo "1";
					echo ((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']))?"https":"https")."://".
					$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).
					"?reset=".$row['password'];
					exit;
					mail(	$row['Email'], "Richiesta di IMPOSTAZIONE password per ".
							htmlspecialchars($_GET['Email']), "ATTENZIONE... Ricevuta una richiesta di IMPOSTAZIONE della password per ".
							$row['Email'].". Se volete annullare questa richiesta non fate nulla, se invece volete confermala indicate una nuova password in questa pagina: ".
							((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']))?"https":"https")."://".$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."?reset=".$row['password'], $frm);
				}
				// ################################################################################
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== RETRIEVE-ONE */
			if (isset($_GET['action']) && ($_GET['action']=="retrieve" || $_GET['action']=="update") && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$qry = $dbs->query($sql_retrieve1);
				$row = $qry->fetchArray();
			}
			/* ===== UPDATE */
			if (isset($_GET['action']) && $_GET['action']=="update-save") {
				$dbs->exec($sql_update);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			/* ===== DELETE */
			if (isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['RowID']) && !empty($_GET['RowID'])) {
				$dbs->exec($sql_delete);
				echo "<script>window.location='".htmlentities($_SERVER['PHP_SELF'])."'</script>";
				exit;
			}
			$dbs->close(); ?>
			<div class="container-fluid">
				<form method="GET" action="<?=htmlentities($_SERVER['PHP_SELF'])?>">
					<div class="row"><?php
						/* ===== RETRIEVE-ALL ===== */
						if (!isset($_GET['action']) || empty($_GET['action'])) { ?>
							<?php // ################################################################################ ?>

							<table border="1" style="width:100%;" class="table table-sm table-responsive table-striped align-middle table-hover" id="sortTable" data-lang="it">
								<thead><?php // ##### CPS, Fields ?>
									<td class="col-2 d-none d-sm-table-cell">Cognome</td>
									<td class="col-2 d-none d-sm-table-cell">Nome</td>
									<td class="col-2 d-none d-sm-table-cell">Denominazione</td>
									<td class="col-1 d-none d-md-table-cell">Email</td>
									<td class="col-1 d-none d-md-table-cell">Telefono</td>
									<td class="col-1 d-none d-md-table-cell">Username</td>
									<td class="col-1 d-none d-md-table-cell">Password</td>
									<td class="col-1 d-none d-md-table-cell">Gruppo</td>
								</thead>
								<tbody>
									<?php
									include 'ap_sqlite.php';
									$qry = $dbs->query($sql_retrieve2);
									while($row = $qry->fetchArray()) { // ##### CPS, Fields
										echo "<tr class='table-row text-dark text-link'>
											<td class='col-2 d-none d-sm-table-cell'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Cognome']."</a></td>
											<td class='col-2 d-none d-sm-table-cell'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Nome']."</a></td>
											<td class='col-2 d-none d-sm-table-cell'><a href='?action=retrieve&RowID=".$row['rowid']."'>".$row['Denominazione']."</a></td>
											<td class='col-1 d-none d-md-table-cell'>".$row['Email']."</td>
											<td class='col-1 d-none d-md-table-cell'>".$row['Telefono']."</td>
											<td class='col-1 d-none d-md-table-cell'>".$row['username']."</td>
											<td class='col-1 d-none d-md-table-cell'>".substr("*************************",1,strlen($row['password']))."</td>
											<td class='col-1 gruppo'>".$row['Gruppo']."</td>
										</tr>";
									}
									$dbs->close(); ?>
								</tbody>
							</table>
							<script>$('#sortTable').dataTable( { "lengthMenu": [ 5, 10, 50, 100 ], "language": { "decimal": ",", "emptyTable": "Nessun dato disponibile.", "info": "Righe da _START_ a _END_ di _TOTAL_ totali.", "infoEmpty": "Elenco da 0 a 0 di 0 in totale.", "infoFiltered": "(filtro su _MAX_ righe)", "infoPostFix": "", "thousands": ".", "lengthMenu": "Elenca _MENU_ righe", "loadingRecords": "Lettura...", "processing": "Ricerca...", "search": "Cerca:", "zeroRecords": "Nessuna informazione disponibile.", "paginate": { "first": "Primo", "last": "ULtimo", "next": "Succ.", "previous": "Prec." }, "aria": { "sortAscending": ": Ordine crescente", "sortDescending": ": Ordine decrescente" } } } );</script>
							<?php // ----- Create ?>
							<input type="hidden" name="action" value="create"><button class="btn btn-success" type="submit"><i class="fas fa-plus-square"></i> Aggiungi</button>
							<?php
						/* ===== CREATE-UPDATE-DELETE ===== */
						} else { ?>
							<?php // ##### CPS, Cognome x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Cognome">Cognome</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Cognome" name="Cognome" 
										<?=($_GET['action']!="create"?"value='".$row['Cognome']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, Nome x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Nome">Nome</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Nome" name="Nome" 
										<?=($_GET['action']!="create"?"value='".$row['Nome']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, Denominazione x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Denominazione">Denominazione</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Denominazione" name="Denominazione" 
										<?=($_GET['action']!="create"?"value='".$row['Denominazione']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, Email[Validation...] x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Email">Email</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Email" name="Email" 
										pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" placeholder="Indicare un indirizzo Email valido"
										<?=($_GET['action']!="create"?"value='".$row['Email']."'":'').($_GET['action']=="retrieve"?' disabled':'')?> required>
								</div>
							</div>
							<?php // ##### CPS, Telefono x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="Telefono">Telefono</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="Telefono" name="Telefono" 
										<?=($_GET['action']!="create"?"value='".$row['Telefono']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, username x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="username">Nome utente</label>
								<div class="col-sm-10">
									<input class="form-control" type="text" id="username" name="username" 
										<?=($_GET['action']!="create"?"value='".$row['username']."'":'').($_GET['action']=="retrieve"?' disabled':'')?>>
								</div>
							</div>
							<?php // ##### CPS, password[Validation...] x5 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="password">Password</label>
								<div class="col-sm-10">
									<input class="form-control" type="<?=($_GET['action']!='create'?'password':'text')?>" id="password" name="password" 
									    value="<?=($_GET['action']!="create"?$row['password'].'':'L\'utente riceverà una email per impostare la password')?>" data-toggle="password" disabled>
									<button type="button" id="togglePasssword" style="height: 0px; width: 0px; padding: 0px; margin: 0px; position: absolute; top: 8px; right: 42px; color: #888;">
										<i class="fa fa-eye-slash"></i></button>
									<script>
										pwd = document.querySelector('#password');
										button = document.querySelector('#togglePasssword');
										button.addEventListener('click', (e) => {
											if(pwd.type == 'password'){
												e.target.setAttribute('class', 'fa fa-eye');
												pwd.type = 'text';
											} else {
												e.target.setAttribute('class', 'fa fa-eye-slash');
												pwd.type = 'password';
											}
										});
									</script>
								</div>
							</div>
							<?php // ##### CPS, Gruppi[idGruppo(Descrizione)->rowid(Descrizione)] x4+3 ?>
							<div class="row g-2 align-items-center">
								<label class="col-sm-2 col-form-label" for="idGruppo">Gruppo</label>
								<div class="col-sm-10">
									<select class="form-control" id="idGruppo" name="idGruppo" <?=($_GET['action']!='retrieve'?'':'disabled')?>>
										<option value='0'>Scegli...</option>
										<?php
											include 'ap_sqlite-row.php';
											$sql2 = "SELECT rowid, * FROM ap_gruppi"; // ##### CPS, Retrieve
											$qry2 = $dbs2->query($sql2);
											while($row2 = $qry2->fetchArray()) {
												echo "<option value='".$row2['rowid']."'".
												(($_GET['action']!='create' && $row['idGruppo']==$row2['rowid'])?" selected":"").">".
												$row2['rowid'].") ".$row2['Descrizione']."</option>";
											}
											$dbs2->close();
										?>
									</select>
								</div>
							</div>

							<?php // ################################################################################ ?>
							<p>&nbsp;</p>
							<?php   // ----- SUBMIT, create-save
							if ($_GET['action']=='create') { ?>
								<input type="hidden" name="action" value="create-save"><button class="btn btn-success" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update-save
							if ($_GET['action']=='update') { ?>
								<input type="hidden" name="RowID" value="<?=$row['rowid']?>"><input type="hidden" name="action" value="update-save"><button class="btn btn-primary" type="submit"><i class="fa fa-check-square"></i> Salva</button>
							<?php } // ----- SUBMIT, update
							if ($_GET['action']!='update' && $_GET['action']!='create') { ?>
								<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>?action=update&RowID=<?=$row['rowid']?>" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp;Modifica</a>
							<?php   // ----- SUBMIT, Alert delete ?>
								<a href="javascript: confirm_delete(<?=$row['rowid']?>);" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Elimina</a>
								<script type="text/javascript">
								function confirm_delete( rid, uid ) {
									if (confirm('Eliminare la riga '+ rid + ' ?')) {
										window.location.href = '<?=htmlentities($_SERVER['PHP_SELF'])?>?action=delete&RowID=' + rid;
									}
								}
								</script>
							<?php } // ----- Back ?>
							<a href="<?=htmlentities($_SERVER['PHP_SELF'])?>" class="btn btn-secondary"><i class="fas fa-arrow-circle-left"></i>&nbsp;Ritorna</a>
						<?php } ?>
					</div>
				</form>
			</div>

	<?php include 'ap_footer.php'; ?>
