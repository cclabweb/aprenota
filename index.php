<?php
/* ================================================================================
 * Web App "Progetto AmbienteParco" | Code name: PCS_PAP_2021
 * --------------------------------------------------------------------------------
 * Index/Home script for:
 * - Login, ask email (as username) and password, then redirect to another page
 * - Reset password, with only email address and confirmation URL sended to email
 * - Logout, destroy session data and logut user to home page
 * --------------------------------------------------------------------------------
 * CPSoft, 1989-2021. - ocdl.it/cw - Released 2020-09-26 - Updated 2021-05-07 18.42
 * Released under GNU/GPL 3.0 and, in one way complaint, Creative Commons BY-SA 4.0
 * ============================================================================= */
	$now = DateTime::createFromFormat('U.u', microtime(true))->setTimezone(new DateTimeZone('Europe/Rome')); // setlocale (LC_TIME, "it_IT");
	$dtm = $now->format("Y-m-d H.i.s.uP");
	$act = "weblog"; // (isset($_GET['username']) ? "accesso" : (isset($_GET['registra']) ? "registra" : "lettura"));
	$uid = (isset($_SESSION['email']) ? $_SESSION['email'] : 'anonimo'); // (isset($_GET['email']) ? $_GET['email'] : (isset($_GET['email']) ? $_GET['email'] : 'anonimo')));
	$ipa = $_SERVER['REMOTE_ADDR'];
	$ref = $_SERVER['HTTPS_REFERER'];
	$bua = $_SERVER['HTTPS_USER_AGENT'];
	$dat = $_SERVER['HTTPS_ACCEPT_LANGUAGE'];
	$ref = (isset($ref) ? $ref : $_SERVER['HTTP_REFERER'] );
	$bua = (isset($bua) ? $bua : $_SERVER['HTTP_USER_AGENT'] );
	$dat = (isset($dat) ? $dat : $_SERVER['HTTP_ACCEPT_LANGUAGE'] );
	$log = $dtm."\t"."\t".$ipa."\t".$ref."\t".$bua."\t".$dat."\r\n";
	$frm = "From: \"PAP\" <pap@ocdl.it>"."\r\n"."Reply-To: <pap@ocdl.it>"."\r\n"."X-Mailer: PHP/".phpversion();

	// ########## MD5 encode something...
	if (isset($_GET['md5']) ) {
		echo md5($_GET['md5']);
		exit;
	}
	// ########## If user asked for logout then destroy Session
	if (isset($_GET['logout'])) {
		$act = "Logout";
		$uid = $_SESSION['Email'];
		$sql = "INSERT INTO ap_weblog_weblog (datetime, action, user, ipaddress, referrer, useragent, data) 
				VALUES ('$dtm', '$action', '$uid', '$ipa', '$ref', '$bua', '$dat')";
		$dbs = new SQLite3('database.sqlite');
		$dbs->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
		$dbs->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
		$dbs->exec($sql);
		$dbs->close();
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}
		session_start();
		session_destroy();
		header("Location: ".$_SERVER['PHP_SELF']);
       	exit;
	} else {
		session_start();
	}

	// ########## Verify login credential for authorized user
	$msg = "";
	$dbs = new SQLite3('database.sqlite');
	$dbs->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
	$dbs->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
	if ( isset($_GET['Email']) && !empty($_GET['Email']) && isset($_GET['Password']) && !empty($_GET['Password']) ) {
		// ########## If submited email and password...
		$sql = "SELECT rowid, *
				  FROM ap_operatori
				 WHERE Email = '".htmlspecialchars($_GET['Email'])."' AND Password = '".( (isset($_GET['reset']) && !empty($_GET['reset'])) ? htmlspecialchars($_GET['reset']) : md5($_GET['Password']) )."'"; // Create a query for identical email and md5 of password
		$qry = $dbs->query($sql);
		$row = $qry->fetchArray();
		if ( $row > 0 && isset($_GET['reset']) && !empty($_GET['reset']) ) {
			// ########## User found but this is a reset answer...
			$sql = "UPDATE ap_operatori
					   SET Password = '".md5($_GET['Password'])."'
					 WHERE Email = '".htmlspecialchars($_GET['Email'])."' AND Password = '".htmlspecialchars($_GET['reset'])."'";
			$dbs->exec($sql);
			mail("reset.pap@ocdl.it", "PAP, eseguito reset per ".htmlspecialchars($_GET['Email'])." da ".htmlspecialchars($_GET['reset'])." a ".md5($_GET['Password']), $log, $frm);
			$act = "Reset";
		}
		if ($row > 0) {
			// ########## If found at last a single record... (that's unique cause the Email is unique in DB)
			$_SESSION['idOperatore']=$row['rowid']; // Keep idOperatore in Session
			$_SESSION['Email']=$row['Email']; // Keep Email in Session
			$_SESSION['Ruolo']=$row['idRuolo']; // Keep Rule in Session
			$_SESSION['Nome']=$row['Nome']; // Keep Nome in Session
			$_SESSION['Cognome']=$row['Cognome']; // Keep Cognome in Session
			$act = ( $row['Ruolo'] > 0 ? "Altro" : "Amministratore" );
		} else {
			// ########## Submited email and password are invalid, alert message in $msg
			$msg = "Nome utente o password non corretti.";
			mail("login.pap@ocdl.it", "PAP, login fallito per email ".$msg." ".htmlspecialchars($_GET['Email']), $log, $frm);
			$act = "Failed";
		}
	} else {
		if ( isset($_GET['Email']) && !empty($_GET['Email']) && isset($_GET['Password']) && empty($_GET['Password']) ) {
			// ########## If submited only email, may be a reset request ?
			$sql = "SELECT rowid, *
					  FROM ap_operatori
					 WHERE Email = '".htmlspecialchars($_GET['Email'])."'";
			$qry = $dbs->query($sql);
			$row = $qry->fetchArray();
			if ($row > 0 && $row['Email'] === htmlspecialchars($_GET['Email'])) {
				// ########## If found a record, sending an email to request reset confirmation
				$msg =	"Inviata una email con la richiesta di conferma di RESET PASSWORD per ".$row['Email'].". ".
						"Per confermare, aprire il collegamento indicato nella email.";
				mail(	$row['Email'], "PAP, Richiesta di conferma per RESET PASSWORD per ".
						htmlspecialchars($_GET['Email']), "ATTENZIONE... PAP ha ricevuto una richiesta di RESET della Password per ".
						$row['Email'].". Se volete annullare questa richiesta non fate nulla, se invece volete confermala indicate una nuova Password in questa pagina: ".
						((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']))?"https":"https")."://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?reset=".$row['Password'], $frm);
				$act = "reset-request";
			}
		}
	}

	// ########## Log every access to the webapp...
	$dbs = new SQLite3('database.sqlite');
	$dbs->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
	$dbs->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
	$sql = "INSERT INTO ap_weblog (datetime, action, user, ipaddress, referrer, useragent, data) 
			VALUES ('$dtm', '$action', '$uid', '$ipa', '$ref', '$bua', '$dat')";
	$dbs->exec($sql);
	$dbs->close();

	if (isset($_SESSION['Email']) && !empty($_SESSION['Email'])) {
		// ########## Login correct, go to Scadenze!
		header('location: ap_profilo.php'); // CPS, Check <= ap_profilo.php
		exit;
	} else {
		// ########## Nobody logged in, ask for Password or Reset
		include 'ap_header.php'; ?>
			<title>Progetto AmbienteParco | <?=$_SESSION['idOperatore']?></title>
			<style>
				#loginForm {
					width: 240px;
					position: absolute;
					top: 50%;
					left: 50%;
					transform: translate(-50%, -50%);
					font-size: 1.2rem
				}
				.submit-center { text-align: center; }
				.logo { max-width: 95%; }
				@media (max-width: 500px) {
					#loginForm { min-width: 90%; }
					.logo { margin-left: 0px; }
				}
			</style>
		</head>
		<body>
			<div class="container-fluid">
				<?=($msg == "" ? "" : "<div class='alert alert-danger'>".$msg."</div>")?>
				<form method="GET" action="<?=htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES, 'utf-8')?>" id="loginForm">
					<div class="row">
						<img src="img/logo-square.png" class="logo"><?php
						if ( $act != "reset-request" ) {
							if ( isset($_GET['reset']) && !empty($_GET['reset']) ) { 
								$dbs = new SQLite3('database.sqlite');
								$dbs->busyTimeout(5000); // 2019-05-17. To mitigate database lock on concurrency
								$dbs->exec('PRAGMA journal_mode = wal;'); // 2019-05-17. WAL mode has better control over concurrency. https://www.sqlite.org/wal.html
								$sql = "SELECT rowid, *
										  FROM ap_operatori
										 WHERE Password = '".htmlspecialchars($_GET['reset'])."'";
								$qry = $dbs->query($sql);
								$row = $qry->fetchArray();
								// ########## Check if requested verification is a valid reservation or not...
								if ( $row > 0 ) { ?>
									<input type="hidden" name="reset" value="<?=htmlspecialchars($_GET['reset'])?>" />
									<input type="hidden" name="Email" value="<?=$row['Email']?>" />
									<div class="form-group">
										<label for="Email-label">Email</label>
										<input type="text" name="Email-label" value="<?=$row['Email']?>" disabled />
									</div>
									<div class="form-group">
										<label for="Password">Password</label>
										<input type="password" id="Password1" name="Password" 
											placeholder="NUOVA PASSWORD (8-16 caratteri: 1 maiuscola, 1 minuscola, 1 numero, 1 segno #_+-$%!?)" data-toggle="password" required>
										<button type="button" id="togglePasssword1" style="height: 0px; width: 0px; padding: 0px; margin: 0px; position: absolute; top: 42px; left: 224px; color: #888;">
											<i class="fa fa-eye-slash"></i></button>
										<script>
											pwd = document.querySelector('#Password1');
											button = document.querySelector('#togglePasssword1');
											button.addEventListener('click', (e) => {
												if(pwd.type == 'password'){
													e.target.setAttribute('class', 'fa fa-eye');
													pwd.type = 'text';
												} else {
													e.target.setAttribute('class', 'fa fa-eye-slash');
													pwd.type = 'password';
												}
											});
										</script>
										<!-- label for="Password">Password</label>
										<input type="password" name="Password" placeholder="NUOVA PASSWORD (8-16 caratteri: 1 maiuscola, 1 minuscola, 1 numero, 1 segno #_+-$%!?)"
										pattern="(?=.*\d.*)(?=.*[a-z].*)(?=.*[A-Z].*)(?=.*[!#_+\-\$%&\?].*).{8,16}" / -->
									</div>
									<div class="form-group submit-center">
										<button type="submit" class="btn btn-primary">Cambia Password</button>
									</div><?php
								} else {
									mail("reset.pap@ocdl.it", "PAP, annullato reset richiesto per ".$row['Email']." (".htmlspecialchars($_GET['reset']).")", $log, $frm);
								}
								$dbs->close();
							} else {
								// ########## Ask for Email and Password! ?>
								<div class="row g-2 align-items-center">
									<div class="form-group col-sm-10">
										<label for="Email">Email</label>
										<input type="text" name="Email" placeholder="Indirizzo EMail" style="display:block;" />
									</div>
									<div class="form-group col-sm-10">
										<label for="Password">Password</label>
										<input type="password" id="Password2" name="Password" 
											placeholder="Password" data-toggle="password">
										<button type="button" id="togglePasssword2" style="height: 0px; width: 0px; padding: 0px; margin: 0px; position: absolute; top: 42px; left: 224px; color: #888;">
											<i class="fa fa-eye-slash"></i></button>
										<script>
											pwd = document.querySelector('#Password2');
											button = document.querySelector('#togglePasssword2');
											button.addEventListener('click', (e) => {
												if(pwd.type == 'password'){
													e.target.setAttribute('class', 'fa fa-eye');
													pwd.type = 'text';
												} else {
													e.target.setAttribute('class', 'fa fa-eye-slash');
													pwd.type = 'password';
												}
											});
										</script>
										<!-- label for="Password">Password</label>
										<input type="password" name="Password" placeholder="Password (8-16 caratteri: 1 maiuscola, 1 minuscola, 1 numero, 1 segno #_+-$%!?)"
										pattern="(?=.*\d.*)(?=.*[a-z].*)(?=.*[A-Z].*)(?=.*[!#_+\-\$%&\?].*).{8,16}" style="display:block;" / -->
									</div>
									<div class="form-group submit-center col-sm-10">
										<button type="submit" class="btn btn-primary">Accedi...</button>
									</div>
								</div><?php
							}
						} ?>
					</div>
				</form><?php
		include 'ap_footer.php';
	}
	exit;
